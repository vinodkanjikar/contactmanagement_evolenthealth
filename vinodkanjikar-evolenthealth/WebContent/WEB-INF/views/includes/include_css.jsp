<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Contact Management</title>
 <link href="<c:url value='/resources/vendor/bootstrap/css/bootstrap.min.css'/>" rel="stylesheet"> 
  <!-- Custom fonts for this template-->
  <link href="<c:url value='/resources/vendor/fontawesome-free/css/all.min.css'/>" rel="stylesheet">
  <link href="<c:url value='/resources/vendor/fontawesome-free/css/googleapis.css'/>" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<c:url value='/resources/css/sb-admin-2.min.css'/>" rel="stylesheet">
	<!--   sweetalert_min.css -->
    <link href="<c:url value='/resources/static/css/sweetalert_min.css'/>" rel="stylesheet"> 
    <link rel="icon" href="data:;base64,=">
    <!-- <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/fixedcolumns/3.3.0/css/fixedColumns.dataTables.min.css" rel="stylesheet"> -->
    
    
    
</head>
