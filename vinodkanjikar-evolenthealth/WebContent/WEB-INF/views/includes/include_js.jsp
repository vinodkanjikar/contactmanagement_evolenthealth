 



  <!-- Bootstrap core JavaScript-->
 
  <script src="<c:url value='/resources/vendor/jquery/jquery.min.js'/>" type="text/javascript"></script>
   <script src="<c:url value='/resources/vendor/bootstrap/js/bootstrap.min.js'/>" type="text/javascript"></script>
  <%-- <script src="<c:url value='/resources/vendor/bootstrap/js/bootstrap.bundle.min.js'/>" type="text/javascript"></script> --%>
 
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
 
  <!-- Core plugin JavaScript-->
  <script src="<c:url value='/resources/vendor/jquery-easing/jquery.easing.min.js'/>" type="text/javascript"></script>

  <!-- Custom scripts for all pages-->
  <script src="<c:url value='/resources/js/sb-admin-2.min.js'/>" type="text/javascript"></script>

  
  <!-- Page level custom scripts -->
   <script src="<c:url value='/resources/static/js/basicValidation.js'/>"></script>     
  <script src="<c:url value='/resources/static/js/captcha.js'/>"></script>
  <!--  Alert message script -->
    <script src="<c:url value='/resources/static/js/sweetalert.min.js'/>"></script> 
    
<!-- data table      -->
<script
		src="<c:url value='/resources/vendor/datatables/jquery.dataTables.js'/>"
		type="text/javascript"></script>
	<script
		src="<c:url value='/resources/vendor/datatables/dataTables.bootstrap4.js'/>"
		type="text/javascript"></script>
	<!-- <script
		src="https://cdn.datatables.net/fixedcolumns/3.3.0/js/dataTables.fixedColumns.min.js"
		type="text/javascript"></script>	 -->
    <%@include file="browser.jsp" %>