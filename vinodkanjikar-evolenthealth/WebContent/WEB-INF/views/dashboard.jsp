<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<style>
.anyClass {
  height:300px;
  overflow-y: scroll;
}
</style>
<%@include file="./includes/include_css.jsp"%>
<body id="page-top">
	<%@include file="dashboardContent.jsp"%>
	<%@include file="./includes/include_js.jsp"%>
	 <div class="loader" ></div>
	<script type="text/javascript">
		$(document).ready(function() {
			var table = $('#myTable').DataTable({
				//default is lfrtip 
			    dom: 'frtlip' 
			}); 
			//add margin to the right and reset clear
			$(".dataTables_length").css('clear', 'none');
			$(".dataTables_length").css('margin-right', '20px');

			//reset clear and padding
			$(".dataTables_info").css('clear', 'none');
			$(".dataTables_info").css('padding', '0');
			
			var msg = '${message}';
			if (msg != "") {

				swal({
					html : true,
					title : '<i></i>',
					text : '<b wrap="hard">' + msg + '</b>'
				});
			}
			$(".loader").fadeToggle("slow");
		});
		
 	</script>

</body>

</html>
