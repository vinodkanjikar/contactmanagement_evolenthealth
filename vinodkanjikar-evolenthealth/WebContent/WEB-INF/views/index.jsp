<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<%@include file="./includes/include_css.jsp"%>


<body class="bg-gradient-primary" id="page-top">

	<div class="container" style="text-align:center">

		<!-- Outer Row -->
		<div class="row justify-content-center" style="margin-top: 100px;">

			<div class="col-xl-10 col-lg-12 col-md-9">

				<div class="card o-hidden border-0 shadow-lg my-5">
					<div class="card-body p-0">
						<!-- Nested Row within Card Body -->
						<div class="row">
							<div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
							<div class="col-lg-6">
								<div class="p-5">
									<div class="text-center">
										<h2 class="h2 text-gray-900 mb-4"> 
											<B>CONTACT MANAGEMENT</B></h2>
									</div>
									<form action="${loginUrl}" class="user" method="post"
										name="myForm" id="myForm" onsubmit="return validationLogin();">
										<c:if test="${param.error != null}">
											<div class="alert alert-danger">
												 ${SPRING_SECURITY_LAST_EXCEPTION.message} 
											</div>
										</c:if>
										<c:if test="${param.logout != null}">
											<div class="alert alert-success">
												 You have been logged out successfully. 
											</div>
										</c:if>
										<input type="hidden" name="${_csrf.parameterName}"
											value="${_csrf.token}" />
										<div class="form-group"> 
										 <input
												type="text" class="form-control form-control-user" id="username" name="ssoId"
												placeholder="User Id" autocomplete=off  
												ondragstart="return false" onselectstart="return false"
												onpaste="return false;" onCopy="return false"
												onCut="return false" onDrag="return false"
												onDrop="return false" />
										</div>
										<div class="form-group row"> 
										<div class="col-sm-11 mb-3 mb-sm-0"> 
											 <input type="password" class="form-control form-control-user" id="password"
												name="password" placeholder="Password" required=""
												autocomplete=off ondragstart="return false"
												onselectstart="return false" onpaste="return false;"
												onCopy="return false" onCut="return false"
												onDrag="return false" onDrop="return false"
												onblur="setFocus();" maxlength="14" /> 
										</div> 
										<div  class="col-sm-1"> 
										<span class="fa fa-fw fa-eye field-icon toggle-password pull-right  form-control-user"></span>
										</div>
										</div>
										 
										<div class="form-group row"> 
										<div class="col-sm-10 mb-3 mb-sm-0"> 
											<input type="text" name="mainCaptcha" id="mainCaptcha1"
												size="28" maxlength="7" class="form-control form-control-user" required="required"
												style="background-color: aqua; background-image: url('resources/static/images/captcha.jpg');  font-style: oblique; font-family: monospace; font-weight: bold; font-size: 21px; text-align: center;"
												disabled="disabled"> 
										</div>  
										<div class="col-sm-2">
										<input type="button"  class="btn btn-primary pull-right block "
												id="refresh" value="Refresh" onclick=" return Captcha();"/>
										</div>
										</div>
										<div class="form-group">
											<input type="text"
												style="background-color: #FAFFBD;"
												name="txtInput" id="txtInput1" maxlength="7"
												placeholder="  Enter Above Captcha" class="form-control form-control-user"
												onpaste="return false;" onCopy="return false"
												onCut="return false" onDrag="return false"
												onDrop="return false" autocomplete=off tabindex="3"
												 >
										</div><hr>
										<div class="form-group">
										<input type="hidden" id="captcha" name="captcha"> 
										<button type="submit" class="btn btn-primary pull-right block" >Login</button>
										</div>
										<br>	
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

		</div>

	</div>
	
	<%@include file="./includes/include_js.jsp"%>

<script type="text/javascript">
$(document).ready(function(){ 
		Captcha(); 
});

$(".toggle-password").click(function() {
	   $(this).toggleClass("fa-eye fa-eye-slash");
	   var password=document.getElementById("password");
		if(password.type=='text')
			{
				password.type='password';
			}
		else 
			{
				password.type='text';
			}
	   });

var message='${message}';
if(message !="")
{
	swal({
		html : true,
		title : '<i></i>',
		text : '<b wrap="hard">'+message+'</b>'
	});
}

</script>

<!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
</body>

</html>
