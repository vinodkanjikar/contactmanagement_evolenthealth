<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!-- Page Wrapper -->
<div id="wrapper">
	<!-- left menu navigation start-->
	<!-- Sidebar -->
	<%@include file="./includes/leftMenu.jsp"%>
	<!-- End of Sidebar -->
	<!-- left menu navigation end-->
	<div id="content-wrapper" class="d-flex flex-column">
		<!-- Main Content -->
		<div id="content">
			<%@include file="./includes/header.jsp"%>
			<!-- Begin Page Content -->
			<div class="container-fluid">
				<!-- Page Heading -->
				<div
					class="d-sm-flex align-items-center justify-content-between mb-4">
					<h2 class="h2 mb-0 text-gray-800"><b>Dashboard</b></h2>
					<!-- <a href="#"
							class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
							class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
				</div>
				<br>
				<div class="form-group bg-white">
					<form:form class="user" action="" method="post"
						modelAttribute="contactDto" id="addContactForm"
						name="addContactForm" enctype="multipart/form-data">
						<form:input type="hidden" path="id" name="id"/>
						<form:input type="hidden" path="optVersion" name="optVersion"/>
						<br>
						<div class="form-group row">
						<div class="col-sm-1 mb-1 mb-sm-0"></div>
							<div class="col-sm-3 mb-3 mb-sm-0">
								<form:input path="firstName"
									class="form-control form-control-user" placeholder="First Name"
									id="firstName" onblur="allowOnlyChar('firstName','firstNameSpan')"
									maxlength="250" data-toggle="tooltip" title="First Name" />
									<span id="firstNameSpan" style="color:red"></span>
							</div>
							<div class="col-sm-3 mb-3 mb-sm-0">
								<form:input path="middleName"
									class="form-control form-control-user"
									placeholder="Middle Name" id="middleName"
									onblur="allowOnlyCharButOptional('middleName','middleNameSpan')" maxlength="250"
									data-toggle="tooltip" title="Middle Name" />
									<span id="middleNameSpan" style="color:red"></span>
							</div>
							<div class="col-sm-3 mb-3 mb-sm-0">
								<form:input path="lastName"
									class="form-control form-control-user" placeholder="Last Name"
									id="lastName" onblur="allowOnlyChar('lastName','lastNameSpan')"
									maxlength="250" data-toggle="tooltip" title="Last Name" />
									<span id="lastNameSpan" style="color:red"></span>
							</div>
						</div>
						<div class="form-group row">
						<div class="col-sm-1 mb-1 mb-sm-0"></div>
							
							<div class="col-sm-3 mb-3 mb-sm-0">
								<form:input path="contactNumber"
									class="form-control form-control-user" placeholder="Mobile No"
									id="contactNumber" onblur="validateNumberOnly('contactNumber','contactNumberSpan')"
									maxlength="10" data-toggle="tooltip" title="Mobile No" />
									<span id="contactNumberSpan" style="color:red"></span>
							</div>
							<div class="col-sm-3 mb-3 mb-sm-0">
								<form:input type="email" path="emailId"
									class="form-control form-control-user" placeholder="Email Id"
									id="emailId" onblur="validateEmailAddress('emailId','emailIdSpan')" maxlength="250"
									data-toggle="tooltip" title="Email Id" />
									<span id="emailIdSpan" style="color:red"></span>
							</div>
						</div>
						
						<div class="form-group row" align="right">
							<div class="col-sm-6 mb-3 mb-sm-0">
								<button class="btn btn-primary" type="reset"
									onclick="resetContact()">Reset</button>
								<button class="btn btn-success" type="button" id="save" name="save"
									onclick="addContact()">Save</button>
									
							</div>
							<div class="col-sm-6 mb-3 mb-sm-0"></div>
						</div>
						<div class="table-responsive row col-sm-12 anyClass " style="border-style: groove">
							<br><br>
							<table id="myTable"
								class="table table-bordered table-striped wrap">
								<thead>
									<tr>
										<th>Sr.No.</th>
										<th>First Name</th>
										<th>Middle Name</th>
										<th>Last Name</th>
										<th>Email Id</th>
										<th>Phone Number</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${contactList}" var="contact"
										varStatus="loop">
										<tr>
											<td>${loop.index+1}</td>
											<td>${contact.firstName}</td>
											<td>${contact.middleName}</td>
											<td>${contact.lastName}</td>
											<td>${contact.emailId}</td>
											<td>${contact.mobileNumber}</td>
											<td><a onclick='editContactDetail(${contact.id})'>Edit</a>
												<a onclick='deleteContactDetail(${contact.id})'>Delete</a></td>
										</tr>
									</c:forEach>
								</tbody>
								<tfoot>
								</tfoot>
							</table>
						</div>
					</form:form>
				</div>
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- End of Main Content -->
		<!-- Footer -->
		<%@include file="./includes/footer.jsp"%>
		<!-- End of Footer -->
	</div>
	<!-- End of Content Wrapper -->
</div>
<!-- End of Page Wrapper -->