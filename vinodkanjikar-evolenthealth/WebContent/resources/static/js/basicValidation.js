
function alphabetSpaceFullNameCheck(lastName) {

	var lastName = document.getElementById(lastName);
	var trimValue = lastName.value.trim();
	var lastNameValue = trimValue;
	var space = lastNameValue.indexOf(" ");
	var name = lastNameValue.substring(0, space).toUpperCase();
	var arr = new Array('AH', 'AI', 'AL', 'AN', 'AO', 'AR', 'AS', 'BE', 'BI',
			'BO', 'BP', 'CH', 'CY', 'DA', 'DE', 'DO', 'EE', 'EK', 'EM', 'ES',
			'FA', 'FE', 'FK', 'FU', 'GI', 'GO', 'GU', 'HA', 'HE', 'HO', 'HU',
			'ID', 'IK', 'IL', 'IN', 'JE', 'JI', 'JO', 'JR', 'JU', 'KA', 'KC',
			'KE', 'KH', 'KI', 'KJ', 'KO', 'KS', 'KU', 'LE', 'LI', 'LO', 'LU',
			'MA', 'MU', 'NA', 'NG', 'OH', 'OM', 'ON', 'PI', 'PI', 'PT', 'QI',
			'RU', 'SA', 'SE', 'SI', 'SM', 'SU', 'TA', 'TI', 'TO', 'TU', 'UL',
			'UR', 'WO', 'WU', 'YE', 'YH', 'YI', 'YJ', 'YO', 'YU', 'ZU');
	var letters = /^[a-z.'A-Z\s]+$/;

	if(space!=trimValue.length)
		{
		swal({
			html : true,
			title : '<i></i>',
			text : '<b>Space not allow in Name.</b>'
		});
		}
	if (lastNameValue == "") {
		swal({
			html : true,
			title : '<i></i>',
			text : '<b>Please enter the Full Name.</b>'
		});
		lastName.value = "";
		return false;

	} else if (/^\s*$/.test(lastNameValue)) {
		swal({
			html : true,
			title : '<i></i>',
			text : '<b>Please enter the Full Name.</b>'
		});
		lastName.value = "";

		return false;
	} else if (!(lastNameValue.match(letters))) {

		swal({
			html : true,
			title : '<i></i>',
			text : '<b>Please enter the Full Name.</b>'
		});
		lastName.value = "";
		return false;
	}

	 else // change done
	{
		for ( var i = 0; i < arr.length; i++) {
			if (arr[i] == name) {

				swal({
					html : true,
					title : '<i></i>',
					text : '<b>Please enter the Full Name.</b>'
				});
				lastName.value = "";
				return false;

			}
		}

	}

}
function allowOnlyChar(id,span) {
	 var letter = /^[a-zA-Z\s]+$/;
	 var name=document.getElementById(id).value;
	 var space = name.indexOf(" ");
	 if(space!=-1 && space!=name.length)
		{
		 document.getElementById(span).innerHTML="Space is not allow";
			//result==false?false:true;
		 return false;
		}
	if (name== "") {
		 document.getElementById(span).innerHTML="Please enter value";
		return false;
	} else if (!(letter.test(name))) {
		document.getElementById(span).innerHTML="Only character are allow";
		return false;
	}
	else
		{
		document.getElementById(span).innerHTML="";
		}
		return true;
}
function allowOnlyCharButOptional(id,span) {
	 var letter = /^[a-zA-Z\s]+$/;
	 var name=document.getElementById(id).value;
	 var space = name.indexOf(" ");
	 if(space!=-1 && space!=name.length)
		{
		 document.getElementById(span).innerHTML="Space is not allow";
		 return false;
		}
	if (name== "") {
		 document.getElementById(span).innerHTML="";
		return true;
	} else if (!(letter.test(name))) {
		document.getElementById(span).innerHTML="Only character are allow";
		return false;
	}
	else
		{
		document.getElementById(span).innerHTML="";
		}
		return true;
}
 
//validate email address
function validateEmailAddress(id,span)
{
	var mai =document.getElementById(id).value;
	var emailId1 = mai;
	var trimValue = emailId1.trim();// change
	var emailId = trimValue; // change
	var email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var str = emailId.slice(emailId.indexOf('@'));
	var count = (str.match(/\./g) || []).length;
	var arr = new Array('.com', '.net', '.org', '.biz', '.coop', '.info',
			'.museum', '.name', '.pro', '.edu', '.gov', '.int', '.mil', '.ac',
			'.ad', '.ae', '.af', '.ag', '.ai', '.al', '.am', '.an', '.ao',
			'.aq', '.ar', '.as', '.at', '.au', '.aw', '.az', '.ba', '.bb',
			'.bd', '.be', '.bf', '.bg', '.bh', '.bi', '.bj', '.bm', '.bn',
			'.bo', '.br', '.bs', '.bt', '.bv', '.bw', '.by', '.bz', '.ca',
			'.cc', '.cd', '.cf', '.cg', '.ch', '.ci', '.ck', '.cl', '.cm',
			'.cn', '.co', '.cr', '.cu', '.cv', '.cx', '.cy', '.cz', '.de',
			'.dj', '.dk', '.dm', '.do', '.dz', '.ec', '.ee', '.eg', '.eh',
			'.er', '.es', '.et', '.fi', '.fj', '.fk', '.fm', '.fo', '.fr',
			'.ga', '.gd', '.ge', '.gf', '.gg', '.gh', '.gi', '.gl', '.gm',
			'.gn', '.gp', '.gq', '.gr', '.gs', '.gt', '.gu', '.gv', '.gy',
			'.hk', '.hm', '.hn', '.hr', '.ht', '.hu', '.id', '.ie', '.il',
			'.im', '.in', '.io', '.iq', '.ir', '.is', '.it', '.je', '.jm',
			'.jo', '.jp', '.ke', '.kg', '.kh', '.ki', '.km', '.kn', '.kp',
			'.kr', '.kw', '.ky', '.kz', '.la', '.lb', '.lc', '.li', '.lk',
			'.lr', '.ls', '.lt', '.lu', '.lv', '.ly', '.ma', '.mc', '.md',
			'.mg', '.mh', '.mk', '.ml', '.mm', '.mn', '.mo', '.mp', '.mq',
			'.mr', '.ms', '.mt', '.mu', '.mv', '.mw', '.mx', '.my', '.mz',
			'.na', '.nc', '.ne', '.nf', '.ng', '.ni', '.nl', '.no', '.np',
			'.nr', '.nu', '.nz', '.om', '.pa', '.pe', '.pf', '.pg', '.ph',
			'.pk', '.pl', '.pm', '.pn', '.pr', '.ps', '.pt', '.pw', '.py',
			'.qa', '.re', '.ro', '.rw', '.ru', '.sa', '.sb', '.sc', '.sd',
			'.se', '.sg', '.sh', '.si', '.sj', '.sk', '.sl', '.sm', '.sn',
			'.so', '.sr', '.st', '.sv', '.sy', '.sz', '.tc', '.td', '.tf',
			'.tg', '.th', '.tj', '.tk', '.tm', '.tn', '.to', '.tp', '.tr',
			'.tt', '.tv', '.tw', '.tz', '.ua', '.ug', '.uk', '.um', '.us',
			'.uy', '.uz', '.va', '.vc', '.ve', '.vg', '.vi', '.vn', '.vu',
			'.ws', '.wf', '.ye', '.yt', '.yu', '.za', '.zm', '.zw');

	var dot = mai.lastIndexOf(".");
	var dname = mai.substring(dot);
	var index = mai.indexOf("@");
	var beforeSpecialChar = mai.substring(0, index);
	var regex = /(.*[a-zA-Z0-9!#\$%\&*\{|}~?'//`)\(+=_-]){3}/i;

	if (emailId == "") // accept emailid blank also
	{
		document.getElementById(span).innerHTML="Please enter emailId";
		return false;
	} else if (emailId.toLowerCase() == 'na') {
		document.getElementById(span).innerHTML="Please enter valid emailId";
		return false;

	}

	else if (index == -1) {
		document.getElementById(span).innerHTML="Please enter valid emailId";
		return false;
		 
	} else if (count >= 3 || count == 0)
	// Check count of "." after @..should be less than 3
	{
		document.getElementById(span).innerHTML="Please enter valid emailId";
		return false;

	} else if (!email.test(emailId)) {

		document.getElementById(span).innerHTML="Please enter valid emailId";
		return false;
	} else if (!regex.test(beforeSpecialChar)) {
		document.getElementById(span).innerHTML="Please enter valid emailId";
		return false;
	}

	// else {

	for ( var i = 0; i < emailId.length; i++) {

		if (emailId[i].charCodeAt(i) > 127) {
			{
				document.getElementById(span).innerHTML="Please enter valid emailId";
				return false;
			}
		}
	}
	for ( var i = 0; i < arr.length; i++) {
		if (arr[i].indexOf(mai.substring(dot)) >= 0) {
			document.getElementById(span).innerHTML="";
			return true;
		}

	}
	document.getElementById(span).innerHTML="";
	return true;
}

 function validatePhoneNumber(formName,fieldName) {
	var phoneNumber = document.forms[formName][fieldName].value;
	var phoneno = /^\d{10}$/;
	if ((phoneNumber.match(phoneno))) {
		return true;
	} else {
		swal({
			html : true,
			title : '<i></i>',
			text : '<b wrap="hard">Please enter Valid Mobile No.</b>'
		});
		 
		return false;
	}
}
function validatePhoneNumberByValue(phoneNumber) { 
	var phoneno = /^\d{10}$/;
	if ((phoneNumber.match(phoneno))) {
		return true;
	} else {
		swal({
			html : true,
			title : '<i></i>',
			text : '<b wrap="hard">Please enter Valid Mobile No.</b>'
		});
		 
		return false;
	}
}
 
function validateRegisterForm(myForm){ 
	var password = document.forms[myForm]["password"].value;
	var conformPassword = document.forms[myForm]["conformPassword"].value;
	if (password == "") {
		swal({
			html : true,
			title : '<i></i>',
			text : '<b>Please enter password.</b>'
		});
		return false;
	}
	 else if (!(/^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]{8,14}$/g.test(password))) {
		swal({
			html : true,
			title : '<i></i>',
			text : '<b wrap="hard">Please enter valid password. Password should be between 8 to 14 characters. Spaces and ^ are not allowed.</b>'
		});
		password.value = "";
		return false;
	} else if (!checkPassword(password)) {
		swal({
			html : true,
			title : '<i></i>',
			text : '<b wrap="hard">Passsword must contain one upper letter,one lower letter ,one number,one special charactor. Spaces and ^ are not allowed.</b>'
		});
		password.value = "";
		return false;
	} else if (conformPassword == "") {
		swal({
			html : true,
			title : '<i></i>',
			text : '<b wrap="hard">Please re enter password.</b>'
		});
		return false;
	} else if (password != conformPassword) {
		swal({
			html : true,
			title : '<i></i>',
			text : '<b wrap="hard">Passwords dont match please provide same password.</b>'
		});
		return false;
	} 
	else {
		return true;
	}

}
//validate login credentials and captcha.
function validationLogin(){
	var mainCaptcha = document.forms["myForm"]["mainCaptcha"].value;
	var enteredCaptcha = document.forms["myForm"]["txtInput"].value;
	var password = document.forms["myForm"]["password"].value;
	
	if (password == "") {
		swal({
			html : true,
			title : '<i></i>',
			text : '<b>Please enter password.</b>'
		});
		return false;
	}
	
	if (password.length < 8 || password.length > 14) {
		swal({
			html : true,
			title : '<i></i>',
			text : '<b wrap="hard">Please enter valid password. Password should be between 8 to 14 characters.</b>'
		});
		password.value = "";
		return false;
	} 
	
	if(enteredCaptcha != mainCaptcha){
		//alert("inside if");
		swal({
			html : true,
			title : '<i></i>',
			text : '<b wrap="hard">Please enter correct captcha</b>'
		});
		enteredCaptcha.value="";
		document.getElementById("txtInput1").value='';
		return false;
	}
	return true;
}
 
function resetContact(){
	
		$("#firstName").val("");
		$("#middleName").val("");
		$("#lastName").val("");
		$("#contactNumber").val("");
		$("#id").val("");
		$("#optVersion").val("");
		$("#emailId").val("");
		document.getElementById("firstNameSpan").innerHTML="";
		document.getElementById("middleNameSpan").innerHTML="";
		document.getElementById("lastNameSpan").innerHTML="";
		document.getElementById("contactNumberSpan").innerHTML="";
		document.getElementById("emailIdSpan").innerHTML="";
}
function addContact()
{  
	var flag=true;
	 flag=allowOnlyChar('firstName','firstNameSpan')==false?false:flag;
	 flag=allowOnlyCharButOptional('middleName','middleNameSpan')==false?false:flag;
	 flag=allowOnlyChar('lastName','lastNameSpan')==false?false:flag;
	 flag=validateNumberOnly('contactNumber','contactNumberSpan')==false?false:flag;
	 flag=validateEmailAddress('emailId','emailIdSpan')==false?false:flag;
					 if(flag)
						 {
						 var data = {};
							data["firstName"] = $("#firstName").val();
							data["middleName"] = $("#middleName").val();
							data["lastName"] = $("#lastName").val();
							data["contactNumber"] = $("#contactNumber").val();
							data["emailId"] = $("#emailId").val();
							data["id"] = $("#id").val();
							data["optVersion"] = $("#optVersion").val();
						       if(data!=null && data!=""){
						       	$.ajax({
						                     type : "POST",
						                     url : "rest/addContactDetail",
						                     dataType: "json",
						                     headers: {
						                     'Accept': 'application/json',
						                     'Content-Type': 'application/json'
						                     },
						                     data : JSON.stringify(data),
						                     beforeSend: function(){
						                    	    $(".loader").show();
						                    	   },
						                     success : function(response) {

						                           if (response!=null && response.status==1) {
						                        	   var oTable = $('#myTable').dataTable(); 
						                               oTable.fnClearTable(); 
						                               $.each(response.data, function (key, item) {
						                                   oTable.fnAddData([key+1,item.firstName,item.middleName,item.lastName,item.emailId,item.mobileNumber,
						                                	   "<a onclick='editContactDetail("+item.id+")'>Edit</a> &nbsp;&nbsp <a onclick='deleteContactDetail("+item.id+")'>Delete</a>"]);//add new row
						                               });
						                            $("#firstName").val("");
						                       		$("#middleName").val("");
						                       		$("#lastName").val("");
						                       		$("#contactNumber").val("");
						                       		$("#id").val(0);
						                       		$("#optVersion").val(0);
						                       		$("#emailId").val("");
						                       		$("#save").html("Save");
						                        	   swal({
						                                   html : true,
						                                   title : '<i></i>',
						                                   text : '<b>'+response.message+'</b>'
						                            });
						                           } else {
						                                  swal({
						                                         html : true,
						                                         title : '<i></i>',
						                                         text : '<b>'+response.message+'</b>'
						                                  });
						                           }
						                     },
						                     complete:function(data){
						                    	    $(".loader").hide();
						                    	   },
						                     error : function(e) {
						                   	  swal({
						                             html : true,
						                             title : '<i></i>',
						                             text : '<b>Sorry, there is some thing wrong.</b>'
						                          });
						                     }
						              });
						       }
						 }
}
function editContactDetail(contactId)
{  
	var data = {};
	data["id"] = contactId;
       if(data!=null && data!=""){
       	$.ajax({
                     type : "POST",
                     url : "rest/editContactDetail",
                     dataType: "json",
                     headers: {
                     'Accept': 'application/json',
                     'Content-Type': 'application/json'
                     },
                     data : JSON.stringify(data),
                     beforeSend: function(){
                 	    $(".loader").show();
                 	   },
                     success : function(response) {

                           if (response!=null && response.status==1) {
                        	    $("#firstName").val(response.data.firstName);
                        		$("#middleName").val(response.data.middleName);
                        		$("#lastName").val(response.data.lastName);
                        		$("#contactNumber").val(response.data.mobileNumber);
                        		$("#id").val(response.data.id);
                        		$("#optVersion").val(response.data.optiVersion);
                        		$("#emailId").val(response.data.emailId);
                        		$("#save").html("Update");
                           } else {
                                  swal({
                                         html : true,
                                         title : '<i></i>',
                                         text : '<b>'+response.message+'</b>'
                                  });
                           }
                     },
                     complete:function(data){
                 	    $(".loader").hide();
                 	   },
                     error : function(e) {
                   	  swal({
                             html : true,
                             title : '<i></i>',
                             text : '<b>Sorry, there is some thing wrong.</b>'
                          });
                     }
              });
       } 
}
function deleteContactDetail(contactId)
{  
	var data = {};
	data["id"] = contactId;
       if(data!=null && data!=""){
       	$.ajax({
                     type : "POST",
                     url : "rest/deleteContactDetail",
                     dataType: "json",
                     headers: {
                     'Accept': 'application/json',
                     'Content-Type': 'application/json'
                     },
                     data : JSON.stringify(data),
                     beforeSend: function(){
                  	    $(".loader").show();
                  	   },
                     success : function(response) {

                           if (response!=null && response.status==1) {
                        	   var oTable = $('#myTable').dataTable(); 
                               oTable.fnClearTable(); 
                               $.each(response.data, function (key, item) {
                                   oTable.fnAddData([key+1,item.firstName,item.middleName,item.lastName,item.emailId,item.mobileNumber,
                                	   "<a onclick='editContactDetail("+item.id+")'>Edit</a> <a onclick='deleteContactDetail("+item.id+")'>Delete</a>"]);//add new row
                               });
                             $("#firstName").val("");
                       		$("#middleName").val("");
                       		$("#lastName").val("");
                       		$("#contactNumber").val("");
                       		$("#id").val(0);
                       		$("#optVersion").val(0);
                       		$("#emailId").val("");
                       		$("#save").html("Save");
                        	   swal({
                                   html : true,
                                   title : '<i></i>',
                                   text : '<b>Record deleted successfuly.</b>'
                            });
                           } else {
                                  swal({
                                         html : true,
                                         title : '<i></i>',
                                         text : '<b>'+response.message+'</b>'
                                  });
                           }
                     },
                     complete:function(data){
                 	    $(".loader").hide();
                 	   },
                     error : function(e) {
                   	  swal({
                             html : true,
                             title : '<i></i>',
                             text : '<b>Sorry, there is some thing wrong.</b>'
                          });
                     }
              });
       } 
}
function validateNumberOnly(id,span){
	
	var value =document.getElementById(id).value;
	if (value== "")							 
	{ 
		document.getElementById(span).innerHTML="Please enter mobile Number.";
		return false;
	} 

	else
	{
		var mobileformat = /^[6-9]\d{9}$/;
		if(value.match(mobileformat))
		{
			document.getElementById(span).innerHTML="";
			return true;

		}
		else { 
			document.getElementById(span).innerHTML="Please enter valid mobile Number. Start between 6 and 9";
			return false;
		}

	}
}