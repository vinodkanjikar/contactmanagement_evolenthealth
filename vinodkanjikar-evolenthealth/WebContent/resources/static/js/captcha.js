function Captcha() {

	var alpha = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K',
			'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y',
			'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'm',
			'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
	var num = new Array('1', '2', '3', '4', '5', '6', '7', '8', '9');
	var i;
	for (i = 0; i < 6; i++) {
		var a = alpha[Math.floor(Math.random() * alpha.length)];
		var b = num[Math.floor(Math.random() * num.length)];
		var c = alpha[Math.floor(Math.random() * alpha.length)];
		var d = alpha[Math.floor(Math.random() * alpha.length)];
		var e = num[Math.floor(Math.random() * num.length)];
		var f = alpha[Math.floor(Math.random() * alpha.length)];
		var g = alpha[Math.floor(Math.random() * alpha.length)];
	}
	var code = a + b + c + d + e + f + g;
	 
	if(null !=document.getElementById("mainCaptcha"))
	document.getElementById("mainCaptcha").value = code;
	if(null !=document.getElementById("mainCaptcha1"))
	document.getElementById("mainCaptcha1").value = code;
	if(null !=document.getElementById("mainCaptcha2"))
		document.getElementById("mainCaptcha2").value = code;
	
	document.getElementById("captcha").value=code;
	
	//code added by Girishk for inserting captcha in respective table.
	if(null != code && code !=""){
		$.ajax({
	        type : "Get",
	        url : "info/insertCaptcha",
	        async : false,
	         data : {
	               'captcha' : code
	        },
	        success : function(response) {
	        	if(null != response){
	        		//alert("captcha inserted successfully..");
	        	}else{
	        		//alert("Error while inserting captcha");
	        	}
	        },
	        error : function(e) {
	        	//alert("Error while inserting captcha");
	        }
	  });
	}
	
}


function validationlogin() // for user
{

	 
	var user = document.getElementById("username").value;
	var pass =  document.getElementById("password").value;
	
	 
	var username = document.getElementById("username").value;
	var password = document.getElementById("password").value;

	var string1 = document.getElementById("mainCaptcha1").value;
	var string2 = document.getElementById("txtInput1").value;

	 if (user == "") {
	 	 swal({
				html : true,
				title : '<i></i>',
				text : '<b>Please enter Username.</b>'
			});
		username.value = "";
		return false;
	} else if (pass == "") {
		swal({
			html : true,
			title : '<i></i>',
			text : '<b>Please enter Password</b>'
		});
		password.value = "";
		return false;
	} else if (user == pass) {
		swal({
			html : true,
			title : '<i></i>',
			text : '<b>Username and Password must be different</b>'
		});
		password.value = "";		
		return false;
	}  
	else if (!CheckPassword(pass)) {
		swal({
			html : true,
			title : '<i></i>',
			text : '<b>Please enter valid password. Password should be between 8 to 14 characters. Spaces and ^ are not allowed.</b>'
		});
		password.value = "";
		return false;
	} else if (string1 != string2) {
		swal({
			html : true,
			title : '<i></i>',
			text : '<b>Please enter correct captcha.</b>'
		});
		Captcha();
		document.getElementById("txtInput1").value = '';
		return false;
	}
	else {
		return true;
	}
}
function validatePassword() {
	var password = document.forms["myForm"]["password"].value;
	var minMaxLength = /^[\s\S]{8,32}$/;
	var upper = /[A-Z]/;
	var lower = /[a-z]/;
	var number = /[0-9]/;
	var special = /[ !"#$%&'()*+,<=>?@[\\\]^_`{|}~]\-./;
	if (minMaxLength.test(password) && upper.test(password)
			&& lower.test(password) && number.test(password)
			&& special.test(password)) {
		return true;
	} else {
		return false;
	}
}
/*function CheckPassword(password) {
	var paswd = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$/;
	if (password.value.match(paswd)) {
		return true;
	} else {
		return false;
	}
}*/

function setFocus()
{
	$("#txtInput1").focus();
	
}

function loadCaptcha()
{
	Captcha();
}

function showHide()
{
	if(password.type=='text')
		{
			document.getElementById("eye").value="show";
			password.type='password';
		}
	else 
		{
			document.getElementById("eye").value="hide";
			password.type='text';
		}
	
}

 