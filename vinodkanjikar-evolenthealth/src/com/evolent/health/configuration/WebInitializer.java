 package com.evolent.health.configuration;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * @author Sandesh
 *
 */
public class WebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer implements WebApplicationInitializer {

	
	public WebInitializer() {
	
	}
	
	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
		ctx.register(WebConfig.class);
		ctx.setServletContext(servletContext);
		ctx.refresh();
		ServletRegistration.Dynamic servlet = servletContext.addServlet("dispatcher", new DispatcherServlet(ctx));
		servlet.setInitParameter("throwExceptionIfNoHandlerFound", "true");
		servlet.setInitParameter("defaultHtmlEscape", "true");    
		servlet.setLoadOnStartup(1);
		servlet.addMapping("/");
		servlet.setMultipartConfig(ctx.getBean(MultipartConfigElement.class));

			}

	@Override
	protected Class<?>[] getRootConfigClasses() {
		
		return new Class[] { WebConfig.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
			return null;
	}

	
	@Override
	protected String[] getServletMappings() 
	{
			return null;
	}
	
}
