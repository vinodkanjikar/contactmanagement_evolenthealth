package com.evolent.health.configuration;

import java.sql.Timestamp;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.evolent.health.entity.LoginDetail;
import com.evolent.health.entity.RequestMsgLog;
import com.evolent.health.utility.BasicUtil;
import com.evolent.health.utility.MasterUtility;



/**
 * @author Sandesh
 *
 */
public class LoggingInterceptor implements HandlerInterceptor
{

	private static final Logger logger = Logger.getLogger(LoggingInterceptor.class);  
	
	
	MasterUtility masterUtility;
	
	public LoggingInterceptor() {
		// TODO Auto-generated constructor stub
	}
	
	public LoggingInterceptor(MasterUtility masterUtility) {
		// TODO Auto-generated constructor stub
		this.masterUtility = masterUtility ;
	}

	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.HandlerInterceptor#afterCompletion(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, java.lang.Exception)
	 */
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response,Object handler, Exception ex) throws Exception {
			System.out.println("---Request Completed---");
			logger.info("---After Completion Method of Logging Interceptor---");
			
	}

	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.HandlerInterceptor#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)
	 */
	@SuppressWarnings("null")
	@Override
	public void postHandle(	HttpServletRequest request, HttpServletResponse response,Object handler, ModelAndView modelAndView) throws Exception {
		System.out.println("---Method executed---");
		logger.info("---Post Handle method of Logging Interceptor---");
	
	}

	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.HandlerInterceptor#preHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object)
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)throws Exception {

		System.out.println("---Before Method Execution---");
		logger.info("---Inside PreHandle method of LoggingInterceptor---");
		logger.info("---Requested Domain URL---"+request.getRequestURL().toString());		 
		logger.info("---inside If of PreHandle(), Requested and defined URL matched---");
		auditTrail(request,response);
		return true; 
	}
	
	public RequestMsgLog auditTrail(HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		HttpSession session = request.getSession(false);
        if (session != null) {
        	response.setContentType("text/html");
            Cookie cookie = new Cookie("JSESSIONID",null);
            //cookie.setDomain(SSORealm.SSO_DOMAIN);
            cookie.setMaxAge(0);
            cookie.setPath("/login");
            cookie.setComment("EXPIRING COOKIE at " + System.currentTimeMillis());
            response.addCookie(cookie);

        }

        LoginDetail LoginDetail = (LoginDetail) request.getSession().getAttribute("LogInUser");
        RequestMsgLog auditTrailInfo=null;
        if(null!=LoginDetail){
	      auditTrailInfo=new RequestMsgLog();
	        auditTrailInfo.setReqIpImeiNo(request.getRemoteAddr());
	        auditTrailInfo.setReqAction(request.getRequestURI());
	        auditTrailInfo.setOsInfo(System.getProperty("os.name"));
	
	        String LoginDetailAgent=(String)request.getHeader("user-Agent"); 
	        String browserName = null,browserVer = null;
	        if(LoginDetailAgent.contains("Chrome")){ //checking if Chrome
	             String substring=LoginDetailAgent.substring(LoginDetailAgent.indexOf("Chrome")).split(" ")[0];
	             browserName=substring.split("/")[0];
	             browserVer=substring.split("/")[1];
	         }
	        else if(LoginDetailAgent.contains("Firefox")){  //Checking if Firefox
	             String substring=LoginDetailAgent.substring(LoginDetailAgent.indexOf("Firefox")).split(" ")[0];
	             browserName=substring.split("/")[0];
	             browserVer=substring.split("/")[1];
	         }
	         else if(LoginDetailAgent.contains("MSIE")){ //Checking if Internet Explorer
	             String substring=LoginDetailAgent.substring(LoginDetailAgent.indexOf("MSIE")).split(";")[0];
	             browserName=substring.split(" ")[0];
	             browserVer=substring.split(" ")[1];
	         }
	         else if(LoginDetailAgent.contains("rv")){ //checking if Internet Explorer 11
			      String substring=LoginDetailAgent.substring(LoginDetailAgent.indexOf("rv"),LoginDetailAgent.indexOf(")"));
			      browserName="IE";
			      browserVer=substring.split(":")[1];
			 }
	        auditTrailInfo.setBrowserUsed(browserName);
	        auditTrailInfo.setBrowserVersion(browserVer);
	        
	        String osVersion = System.getProperty("os.version");
	        auditTrailInfo.setOsVersion(osVersion);
	        Timestamp timestamp = BasicUtil.getCurrentTimestamp();//new Timestamp(df.parse(df.format(new Date())).getTime());
	        auditTrailInfo.setCreationDate(timestamp);
	        auditTrailInfo.setLoginDetailsId(LoginDetail.getLoginDetailsId());
	        auditTrailInfo.setReqType("W");
	      	if(null !=masterUtility)
	        masterUtility.auditTrailSave(auditTrailInfo); 
        }
        return auditTrailInfo;
	}

	}

