package com.evolent.health.configuration;

import javax.servlet.MultipartConfigElement;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.evolent.health.utility.MasterUtility;

/**
 * @author Sandesh
 *
 */

@Configuration
@EnableWebMvc
@EnableScheduling
@ComponentScan(basePackages = "com.evolent.health")
public class WebConfig extends WebMvcConfigurerAdapter{
	
	@Autowired
	MasterUtility masterUtility;
	
	private static final Logger logger = Logger.getLogger(WebConfig.class);
	
	
	public WebConfig() {

	}

	@Bean
	public MultipartConfigElement multipartConfigElement() {
		logger.info("Multipart Configuretion Element start:");
		
		MultipartConfigFactory factory = new MultipartConfigFactory();
		factory.setMaxFileSize("2000KB");
		factory.setMaxRequestSize("2000KB");
		return factory.createMultipartConfig();
	}

	@Bean(name = "multipartResolver")
	public StandardServletMultipartResolver resolver() {
		return new StandardServletMultipartResolver();
	}
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {

		logger.info("configureViewResolvers Element start:");
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		registry.viewResolver(viewResolver);
	}

	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}

	@Override
	public void configurePathMatch(PathMatchConfigurer matcher) {
		matcher.setUseRegisteredSuffixPatternMatch(true);
	}
	
	
	 @Override
	  	public void addInterceptors(InterceptorRegistry registry) {
	  	    registry.addInterceptor(new LoggingInterceptor(masterUtility));
	  	}
	    
}
