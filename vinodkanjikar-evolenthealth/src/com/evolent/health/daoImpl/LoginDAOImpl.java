package com.evolent.health.daoImpl;


import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.evolent.health.dao.LoginDAO;
import com.evolent.health.entity.Captcha;
import com.evolent.health.entity.LoginDetail;
import com.evolent.health.utility.BasicUtil;
import com.evolent.health.utility.MasterUtility;

/**
 * @author VinodK
 *
 */
@Repository
public class LoginDAOImpl extends HibernateDaoSupport implements LoginDAO 
{
	private static final Logger logger = Logger.getLogger(LoginDAOImpl.class);
	public static final String COLON = ":";
    public static final String SPACE = "";
	private static final String THIS_COMPONENT = LoginDAOImpl.class.getName() + SPACE + COLON + SPACE;
	
	
	static final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	
	@Autowired
	public void getHibernateSession(SessionFactory sessionFactory)
	{
		setSessionFactory(sessionFactory);
	}
	
	@Autowired                           
	MasterUtility utilityMstr; 

	@SuppressWarnings("unchecked")
	@ExceptionHandler
	public LoginDetail findBySSO(String sso) 
	{
		try {
			if(sso !=null && sso !="")
			{
				sso=sso.toLowerCase();
			}
			
			Query query = (Query) getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from LoginDetail where lower(emailId) =:userId");
			query.setParameter("userId", sso.trim());
			List<LoginDetail> userList = query.list();  
			
			if((userList!=null) &&(userList.size()>0))
				return userList.get(0);
			else
				return null;
		} 
		catch (Exception e) 
		{
			logger.error("Exception in class LoginDaoImpl : findBySSO() method ",e);
			
		}
		 
		return null;
		
	}

	 
	@SuppressWarnings("unchecked")
	@Override
	public LoginDetail getUserDetails(String userName) 
	{
		try {
			Query query = (Query) getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from LoginDetail where emailId =:userId");
			query.setParameter("userId", userName.trim());
			List<LoginDetail> userList = query.list();  
			if ((userList!=null)&&(userList.size() != 0))
			{
				return userList.get(0);
			}
			else
				return null;
		} 
		catch (HibernateException e) 
		{
			logger.error("Exception in class LoginDaoImpl : getUserDetails() method :",e);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateLoggingStatus(int userId, int status) {
		try 
		{
			List<LoginDetail> user;
			Query query = (Query) getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from LoginDetail where loginDetailsId =:userId");
			query.setParameter("userId", userId);
			user = query.list();
			if((user!=null)&&(user.size()!=0))
			{
				LoginDetail user1=user.get(0);
				if(status==0) {
					user1.setLastLoggedOutDatetime(BasicUtil.getCurrentTimestamp());
				}
				if(status==1) {
					user1.setLastLoggedInDatetime(BasicUtil.getCurrentTimestamp());
				}
				user1.setLoggedInStatus(status);
				System.out.println("Status=========================="+user1.getLoggedInStatus());
				getHibernateTemplate().getSessionFactory().getCurrentSession().update(user1);
			}
 		}
		catch (Exception e) 
		{
			logger.error(THIS_COMPONENT + "Exception Occurred Inside LoginDaoImpl:updateLoggingStatus():",e);
			e.printStackTrace();
		}
	}
 	@SuppressWarnings("unchecked")
	@Override
	public void updateFailAttempts(String username) {
		System.out.println("updateFailAttempts Starts Here-----------------");
		String login_attempt="";
		int MAX_ATTEMPTS=0;
		try
		{
		
		 login_attempt = new BasicUtil().getPropertyValues("MAX_LOGIN_ATTEMPTS");
		 if(login_attempt!=null)
		 	{ 
			 	MAX_ATTEMPTS = Integer.parseInt(login_attempt);
		 	}
		 else
		 	{
			 logger.info("Error in reading value from property file");
		 	}
		}
		catch(IOException e)
		{
			 logger.error("Error in reading value from property file:Exception Occurred inside updateFailAttempts() :",e);
		}
	
		LoginDetail user = findBySSO(username);
		try{
			if (user != null) {
				if (user.getInvalidLoginAttempt() < MAX_ATTEMPTS) 
				{			 	
					List<LoginDetail> user1;
					Query query = (Query) getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from LoginDetail where emailId =:userId");
					query.setParameter("userId", username.trim());
					user1 = query.list();
					if((user1!=null)&&(user1.size()!=0))
					{
						user1.get(0).setInvalidLoginAttempt(user.getInvalidLoginAttempt()+1);
						getHibernateTemplate().update(user1.get(0));
					}
					
					//List<LoginDetail> user1 = (List<LoginDetail>)getHibernateTemplate().find("from LoginDetail where emailId='"+username+"'");    
					else{
					throw new LockedException("Invalid Username or Password");
					} 		 
				}
			}
		}catch(Exception e){
			logger.error("error in LoginDAOImpl updateFailAttempts method()",e);
		}
		
	}


	 
	@Override
	public void resetFailAttempts(String username) {
		LoginDetail user = findBySSO(username);
		
		if (user != null) {
			getSessionFactory().getCurrentSession().createQuery("update LoginDetail set invalidLoginAttempt=0 where emailId='"+username+"'").executeUpdate();		
		}

		
	}


	 
	@SuppressWarnings("unchecked")
	@Override
	public LoginDetail getUserAttempts(String username)
	{
		System.out.println("LoginDetail Attempts () Starts Here :");
		Query query = (Query) getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from LoginDetail where emailId =:userId");
		query.setParameter("userId", username);
		List<LoginDetail> userList = query.list();  
		
		if((userList!=null) &&(userList.size()>0))
			return userList.get(0);
		else
			return null;
		
		}


	 
	@Override
	public void resetBlockedAccount(String username) {
		System.out.println("resetBlockedAccount Attempts () Starts Here :");
		LoginDetail userAttempts=null;
		String encoded_pwd="";
		@SuppressWarnings("unchecked")
		List<LoginDetail> l=(List<LoginDetail>)getHibernateTemplate().find("from LoginDetail where emailId='"+username+"'");
		if(l.size()>0)
		{
			userAttempts=l.get(0);
		}
		String original_pwd=BasicUtil.generatePassword();
		System.out.println("New password:---------------"+original_pwd);
		encoded_pwd=passwordEncoder.encode(original_pwd);
		userAttempts.setPassword(encoded_pwd);
		userAttempts.setIsForceChangePasswordFlg("Y");
		userAttempts.setLastLockedDatetime(BasicUtil.getCurrentTimestamp());
		userAttempts.setIsLockedFlg("N");
		userAttempts.setInvalidLoginAttempt(0);
		getHibernateTemplate().update(userAttempts);
		//	EmailUtility.sendEmail(email, original_pwd, "change_forgot_Password");	

	}

	@SuppressWarnings("unchecked")
	@Override
	public String insertCaptcha(String captcha, String ip) {
		logger.info("Inside LoginDaoImpl : insertCaptcha() method");
		Captcha capchaObj = new Captcha();
		try{
			Query query = (Query) getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from Captcha where ip =:ip");
			query.setParameter("ip", ip);
			List<Captcha> captchaList = query.list();  
			
			if (null !=captchaList && captchaList.size() <= 0) {
				capchaObj.setCaptcha(captcha);
				capchaObj.setIp(ip);
				getHibernateTemplate().getSessionFactory().getCurrentSession().save(capchaObj);
		    }else{
		    	capchaObj = captchaList.get(0);
		    	capchaObj.setCaptcha(captcha);
				capchaObj.setIp(ip);
				getHibernateTemplate().getSessionFactory().getCurrentSession().update(capchaObj);
		    }
			
		}catch(Exception e){
			logger.error("Exception in LoginDAOImpl insertCaptcha method()",e);
			return "fail";
		}
		return "success";
	} 
	

}
