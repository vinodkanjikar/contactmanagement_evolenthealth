package com.evolent.health.daoImpl;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import com.evolent.health.dao.AbstractDao;
import com.evolent.health.dao.UserDAO;
import com.evolent.health.entity.LoginDetail;



/**
 * @author VinodK
 *
 */
@Repository
public class UserDAOImpl extends AbstractDao<Integer, LoginDetail> implements UserDAO, UserDetailsService {
	private static final Logger logger = Logger.getLogger(UserDAOImpl.class);
	public static final String COLON = ":";
	public static final String SPACE = "";
	private static final String THIS_COMPONENT = UserDAOImpl.class.getName() + SPACE + COLON + SPACE;
	 
	@Override
	public LoginDetail findBySSO(String sso) 
	{

		logger.info(THIS_COMPONENT + "Inside LoginDetail findBySSO ::");

		Criteria criteria = createEntityCriteria();
		LoginDetail user = null;
		try 
		{
			logger.info(THIS_COMPONENT + "Inside LoginDetail findBySSO Inside Try Block ::");
			if (null != sso) 
			{
				logger.info(THIS_COMPONENT + "Inside LoginDetail findBySSO Inside Try Block + IF Block::");
				criteria.add(Restrictions.eq("gudUserName", sso));
			}
			user = (LoginDetail) criteria.uniqueResult();
		}
		catch (Exception e)
		{
			logger.error(THIS_COMPONENT + "Exception Occurred inside UserDAOImpl:findBySSO():", e);
		}
		return user;

	}

	@Override
	public UserDetails loadUserByUsername(String arg0) throws UsernameNotFoundException {
		return null;
	}
}	
