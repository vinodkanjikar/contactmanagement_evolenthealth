/**
 * 
 */
package com.evolent.health.daoImpl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import com.evolent.health.appconstant.AppConstants;
import com.evolent.health.dao.RegistrationDao;
import com.evolent.health.entity.ContactDetails;
import com.evolent.health.utility.BasicUtil;
import com.evolent.health.ws.ContactDetailsDTO;

/**
 * @author VinodK
 *
 */
@Repository
public class RegistrationDaoImpl extends HibernateDaoSupport implements RegistrationDao{

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	public void getHibernateSession(SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ContactDetails> addContactDetails(ContactDetailsDTO detailsDTO) {
		ContactDetails contactDetails=new ContactDetails();
		if(detailsDTO.getId()!=null && detailsDTO.getId()>0) {
			contactDetails=(ContactDetails) sessionFactory.getCurrentSession().get(ContactDetails.class,detailsDTO.getId());
			contactDetails.setModifiedBy(detailsDTO.getCreatedBy());
			contactDetails.setModificationDate(BasicUtil.getCurrentTimestamp());
		}
		else {
			
			List<ContactDetails> list=(List<ContactDetails>) sessionFactory.getCurrentSession().createQuery("from ContactDetails where status=:status and ((firstName=:firstName and lastName=:lastName) or mobileNumber=:mobileNumber)")
			.setParameter("status", AppConstants.ACTIVE)
			.setParameter("firstName", detailsDTO.getFirstName())
			.setParameter("lastName", detailsDTO.getLastName())
			.setParameter("mobileNumber", detailsDTO.getContactNumber()).list();
			if(list.size()>0) {
				
				if(list.get(0).getMobileNumber().equalsIgnoreCase(detailsDTO.getContactNumber()))
				{
					detailsDTO.setFailure(true);
					detailsDTO.setMessage("Mobile Number already exist.");
				}
				else if(list.get(0).getFirstName().equalsIgnoreCase(detailsDTO.getFirstName()) && list.get(0).getLastName().equalsIgnoreCase(detailsDTO.getLastName()))
				{
					detailsDTO.setFailure(true);
					detailsDTO.setMessage("Contact details are already available.");
				}
				
			}
			else
			{
				contactDetails.setCreatedBy(detailsDTO.getCreatedBy());
				contactDetails.setCreationDate(BasicUtil.getCurrentTimestamp());
			}
			
		}
		if(!detailsDTO.isFailure()) {
			contactDetails.setEmailId(detailsDTO.getEmailId());
			contactDetails.setFirstName(detailsDTO.getFirstName());
			contactDetails.setLastName(detailsDTO.getLastName());
			contactDetails.setMiddleName(detailsDTO.getMiddleName());
			contactDetails.setMobileNumber(detailsDTO.getContactNumber());
			contactDetails.setStatus(AppConstants.ACTIVE);
			sessionFactory.getCurrentSession().saveOrUpdate(contactDetails);
		}
		return (List<ContactDetails>) sessionFactory.getCurrentSession().createQuery("from ContactDetails where status=:status")
				.setParameter("status", AppConstants.ACTIVE).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ContactDetails> getContactDetails(ContactDetailsDTO detailsDTO) {
		
		if(detailsDTO!=null && detailsDTO.getId()!=null && detailsDTO.getId()>0) {
			return (List<ContactDetails>) sessionFactory.getCurrentSession().createQuery("from ContactDetails where status=:status and id=:id")
					.setParameter("status", AppConstants.ACTIVE)
					.setParameter("id", detailsDTO.getId()).list();
		}
		else
		{
			return (List<ContactDetails>) sessionFactory.getCurrentSession().createQuery("from ContactDetails where status=:status")
					.setParameter("status", AppConstants.ACTIVE).list();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ContactDetails> deleteContactDetails(ContactDetailsDTO detailsDTO) {
		
		if(detailsDTO.getId()!=null && detailsDTO.getId()>0) {
			ContactDetails details=(ContactDetails) sessionFactory.getCurrentSession().get(ContactDetails.class, detailsDTO.getId());
			details.setStatus(AppConstants.INACTIVE);
			sessionFactory.getCurrentSession().update(details);
		}
		return (List<ContactDetails>) sessionFactory.getCurrentSession().createQuery("from ContactDetails where status=:status")
				.setParameter("status", AppConstants.ACTIVE).list();
	}

	@Override
	public ContactDetails editContactDetails(ContactDetailsDTO detailsDTO) {
		if(detailsDTO.getId()!=null && detailsDTO.getId()>0) {
			return (ContactDetails) sessionFactory.getCurrentSession().get(ContactDetails.class, detailsDTO.getId());	
		}
		else
			return null;
		
	}
 
}


