/**
 * 
 */
package com.evolent.health.appconstant;

/**
 * @author 
 *
 *This class contains application constants.
 */
public class AppConstants { 
	public static final String COLON = ":";
    public static final String SPACE = " ";
	public static final String USER_STATUS ="0";
	public static final String AFTER_LOGOUT_LOGGEDIN_STATUS="0";
	public static final String AFTER_CHANGE_PASSWORD_LOGGEDIN_STATUS="1";
	public static final String USER_LOGGED_IN_STATUS ="2";
	public static final String USER_GENDER_MALE="MALE";
	public static final String USER_GENDER_FEMALE="FEMALE";
	public static final String USER_TITLE_MISS="MISS";
	public static final String USER_TITLE_MR="MR";
	public static final String USER_TITLE_MRS="MRS";
	public static final String SUCCESSFUL_REGISTRATION="You have registered successfully, password is sent to your registered email Id.";
	public static final String ALREADY_REGISTER="You have already registered with this application.";
	public static final String ALREADY_REGISTER_WITH_OTHER = "You have already registered, you can use your old password to login";
	public static final String OTP_MESSAGE="You have entered wrong OTP";
	public static final String BAD_URL_PATTERN= "Bad url pattern";	
	public static final String USER_ID_ALREADY_EXIST ="UserId is already exist, please try with new UserId"; 
	public static final String ACTIVE_STATUS="A";	
	public static final String INACTIVE_STATUS="I";
	public static final String FAILURE_STATUS="F";
	public static final String SUCCESS_STATUS="S";
	public static final String YES="Y";
	public static final String NO="N";
	public static final String DEFAULT_CREATED_BY="Owner";
	public static final String WEB="W";
	public static final int ACTIVE=1;	
	public static final int INACTIVE=0;
}
