package com.evolent.health.service;

import com.evolent.health.entity.LoginDetail;


/**
 * @author VinodK
 *
 */
public interface UserService {
	
 	
	LoginDetail findBySSO(String sso, Integer authStatus);
}