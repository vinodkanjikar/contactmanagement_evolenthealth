package com.evolent.health.service;


import com.evolent.health.entity.LoginDetail;


/**
 * @author VinodK
 *
 */
public interface LoginService {

	LoginDetail findBySSO(String sso);

	LoginDetail getUserDetails(String userName);

	void updateLoggingStatus(int gudUserId, int status);
	
	//New method to restrict login attempt	
		/**
		 * @param username
		 */
		void updateFailAttempts(String username);
		/**
		 * @param username
		 */
		void resetFailAttempts(String username);
		/**
		 * @param username
		 * @return
		 */
		LoginDetail getUserAttempts(String username);
		/**
		 * @param username
		 */
		void resetBlockedAccount(String username);
		
	 //	User checkStatus(String gudUserId);
		
		public String insertCaptcha(String captcha,String ip);

}
