package com.evolent.health.security;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import com.evolent.health.entity.LoginDetail;
import com.evolent.health.service.LoginService;

/**
 * @author VinodK
 *
 */
@Component("authenticationEventListner")
public class AuthenticationEventListener implements AuthenticationEventPublisher {

	private static final Logger logger = Logger.getLogger(AuthenticationEventListener.class);
	public static final String COLON = ":";
    public static final String SPACE = "";
	private static final String THIS_COMPONENT = AuthenticationEventListener.class.getName() + SPACE + COLON + SPACE;
	
	private static final int MAX_ATTEMPTS = 2;
	
	@Autowired
	LoginService loginService;

	@Autowired
	@Qualifier("customUserDetailsService")
	UserDetailsService userDetailsService;

	@Override
	public void publishAuthenticationFailure(AuthenticationException exception,Authentication authentication) {
		String username="";
		try {
			username = authentication.getName().toString();
		} 
		catch (Exception e) {
			logger.error(THIS_COMPONENT + "Exception Occurred inside AuthenticationEventListener:publishAuthenticationFailure():", e);
		}
		 LoginDetail userAttempt = loginService.getUserAttempts(username);
		 int attempts = 0;
		if(userAttempt !=null)
		{
			    if(username !=null )
			    {   
			    		int count= userAttempt.getInvalidLoginAttempt();
			    		if(count<MAX_ATTEMPTS)
			    			loginService.updateFailAttempts(username);
			    		else
			    		{
			    			loginService.resetBlockedAccount(username);
			    			throw new LockedException("User Account is locked!System generated password mail has been sent on registered E-mail ID. Please login with the password provided and change the password.");
			    		}
				/*
				 * } else throw new BadCredentialsException("Bad Credential");
				 */
			    }
			    else
			    {
			    	attempts = userAttempt.getInvalidLoginAttempt();
			        loginService.updateFailAttempts(username);  
			        if (attempts + 1 >= MAX_ATTEMPTS) 
			        {                 
			        	loginService.resetBlockedAccount(username); 
			        }                                   
			      }
		}
		
	}

	@Override
	public void publishAuthenticationSuccess(Authentication authentication) {
		loginService.resetFailAttempts(authentication.getName());
		
	}
	
	
	

}
