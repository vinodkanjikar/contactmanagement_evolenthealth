 package com.evolent.health.security;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import com.evolent.health.entity.LoginDetail;
import com.evolent.health.service.LoginService;


/**
 * @author VinodK
 *
 */
@Component("authenticationProvider")
public class LimitLoginAuthenticationProvider  extends DaoAuthenticationProvider{
	private static final Logger logger = Logger.getLogger(LimitLoginAuthenticationProvider.class);
	public static final String COLON = ":";
    public static final String SPACE = "";
	private static final String THIS_COMPONENT = LimitLoginAuthenticationProvider.class.getName() + SPACE + COLON + SPACE;
	

	@Autowired
	LoginService loginService;

	@Autowired
	@Qualifier("customUserDetailsService")
	UserDetailsService userDetailsService;
	
	@Override
	public void setUserDetailsService(UserDetailsService userDetailsService) {
		super.setUserDetailsService(userDetailsService);
	}

	@Override
	public Authentication authenticate(Authentication authentication)
          throws AuthenticationException {

	  try {

		Authentication auth = super.authenticate(authentication);

		//if reach here, means login success, else an exception will be thrown
		//reset the user_attempts
	//	loginService.resetFailAttempts(authentication.getName());
		loginService.resetBlockedAccount(authentication.getName());
		return auth;

	  } 
	  catch (BadCredentialsException e) {
		  
			logger.error(THIS_COMPONENT + "Exception Occurred inside LimitLoginAuthenticationProvider:authenticate():", e);
		//invalid login, update to user_attempts
		  loginService.updateFailAttempts(authentication.getName());
		throw e;

	  } 
	  catch (LockedException e){

		//this user is locked!
			logger.error(THIS_COMPONENT + "Exception Occurred inside LimitLoginAuthenticationProvider:authenticate():", e);
		String error = "";
		LoginDetail userAttempts =loginService.findBySSO(authentication.getName());
        if(userAttempts!=null){
        	Date lastAttempts = userAttempts.getLastLockedDatetime();
			error = "User account is locked! <br><br>Username : " + authentication.getName() + "<br>Last Attempts : " + lastAttempts;
		}
        else
        {
			error = e.getMessage();
		}

	  throw new LockedException(error);
	}

	}

}
