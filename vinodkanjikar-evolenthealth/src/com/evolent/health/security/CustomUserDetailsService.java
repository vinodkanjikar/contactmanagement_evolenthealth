 package com.evolent.health.security;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.evolent.health.entity.LoginDetail;
import com.evolent.health.service.LoginService;





/**
 * @author VinodK
 *
 */
@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
	static final Logger logger = Logger.getLogger(CustomUserDetailsService.class);
	public static final String COLON = ":";
    public static final String SPACE = "";
	private static final String THIS_COMPONENT = CustomUserDetailsService.class.getName() + SPACE + COLON + SPACE;
	private String yourParameter="";
	/**
	 * 
	 */
	public CustomUserDetailsService() {
		// TODO Auto-generated constructor stub
	}
	
	
	@Autowired
	private LoginService loginService;
	
	
	@ExceptionHandler
	public UserDetails loadUserByUsername(String userName)throws UsernameNotFoundException,DataAccessException {
		/*boolean enabled = true;
		boolean accountNonExpired = true;
		boolean credentialsNonExpired = true;
		boolean accountNonLocked = true;
		*/
		 String[] split = userName.split(":");
		 String ssoId = split[0];
		 String extraInput = split[1];
	
		logger.info(THIS_COMPONENT + "Inside loadUserByUsername Metod Begins::");
		LoginDetail user = loginService.findBySSO(ssoId);
		if(user==null)
		{
			logger.info(THIS_COMPONENT + "Inside loadUserByUsername Metod Inside IF Block::");
			throw new UsernameNotFoundException("Username not found");
		}
		if(extraInput==null)
		{
			logger.info(THIS_COMPONENT + "Inside loadUserByUsername Metod Inside IF Block(No Captcha Found)::");
			throw new UsernameNotFoundException("Invalid Captcha");
		}
		
		/*else if(user.getGudLoggedInStatus()==1)
		{
			logger.info(THIS_COMPONENT + "Inside loadUserByUsername Metod Inside IF Block(No Captcha Found)::");
			throw new ConcurrencyFailureException("User is already logged in some where.");
		}*/
			
		/*return new org.springframework.security.core.userdetails.User(user.getGudUserId(), user.getGudCurPassword(), 
				enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, getGrantedAuthorities(user));*/
		return new org.springframework.security.core.userdetails.User(user.getEmailId(), user.getPassword(), 
				true , true, true, true, getGrantedAuthorities(user));
		
	}

	/*//To check User is already logged in 
	private boolean isEnabled(String gudUserId) {
		User user = loginService.checkStatus(gudUserId);
		if(user==null)
		{
			logger.info(THIS_COMPONENT + "Inside loadUserByUsername Metod Inside IF Block::");
			return false;
		}
		else if(user.getGudLoggedInStatus()==1)
			return false;
		else	
		return true;
	}*/

	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetails#getAuthorities()
	 */
	private List<GrantedAuthority> getGrantedAuthorities(LoginDetail user) {
		logger.info(THIS_COMPONENT + "Inside loadUserByUsername Metod Begins::");
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_Admin"));  //Defined for User Role
		return authorities;
	}

	/*@Override
	public void commence(final HttpServletRequest request, final HttpServletResponse response,
			final AuthenticationException authException) throws IOException, ServletException {

		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		response.addHeader("WWW-Authenticate", "Basic realm=" + getRealmName() + "");

		PrintWriter writer = response.getWriter();
		writer.println("HTTP Status 401 : " + authException.getMessage());
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		setRealmName("MY_TEST_REALM");
		super.afterPropertiesSet();
	}*/

	 public CustomUserDetailsService(HttpServletRequest request) {
	       // super(request);
	        yourParameter = request.getParameter("txtInput");
	    }

	    public String getyourParameter() {
	        return yourParameter;
	    }
	
}







