
 package com.evolent.health.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.authentication.session.CompositeSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.ConcurrentSessionControlAuthenticationStrategy;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionFixationProtectionStrategy;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;


/**
 * @author VinodK
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	@Qualifier("customUserDetailsService")
	UserDetailsService userDetailsService;

	@Autowired
	@Qualifier("authenticationProvider")
	AuthenticationProvider authenticationProvider;
	
	@Autowired
     @Qualifier("authenticationEventListner")
     AuthenticationEventListener authenticationEventListner;
	PersistentTokenRepository tokenRepository;
	
	/*@Autowired
	ExUsernamePasswordAuthenticationFilter exUsernamePasswordAuthenticationFilter;*/
	
	
	@Bean
    public AuthenticationEventPublisher authenticationListener() {
    return new AuthenticationEventListener();
    }
	
	
	
	/**
	 * 
	 */
	public SecurityConfig() {
		// TODO Auto-generated constructor stub
	}


	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {

		auth.userDetailsService(userDetailsService);
		
		 //configuring custom user details service
		auth.authenticationProvider(authenticationProvider);
		
		// configuring login success and failure event listener
        auth.authenticationEventPublisher(authenticationEventListner);
     
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {		
	   // http.requiresChannel().anyRequest().requiresSecure();
		http.authorizeRequests()
		.antMatchers("/").permitAll()
		.antMatchers("/rest/**").permitAll()
		.antMatchers("/userManagementAPI/**").permitAll()
		/*.antMatchers("/resources/**").permitAll()*/
		.antMatchers("/*").access("hasRole('ROLE_Admin')")	 
		//To disable HTTP Methods(OPTIONS,DELETE,HEAD,PUT,TRACE and HEAD)
		.antMatchers(HttpMethod.GET).permitAll()
		.antMatchers(HttpMethod.POST).permitAll()
		.antMatchers(HttpMethod.OPTIONS).denyAll()
		.antMatchers(HttpMethod.DELETE).denyAll()
		.antMatchers(HttpMethod.HEAD).denyAll()
		.antMatchers(HttpMethod.PUT).denyAll()
		.antMatchers(HttpMethod.TRACE).denyAll()
		.antMatchers(HttpMethod.HEAD).denyAll()
		//Ajit for static pages
		.antMatchers("/info/**").permitAll()
		.antMatchers("/userManagement/**").permitAll()
		.and()
		.sessionManagement().sessionAuthenticationStrategy(concurrentSession()).and()
		/*Added for handling login section*/
		.formLogin().loginPage("/login").loginProcessingUrl("/login").defaultSuccessUrl("/login").permitAll()
		.failureUrl("/login?error")
		.usernameParameter("ssoId").passwordParameter("password").and()
	
		.addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class)
		.csrf().and()
		.exceptionHandling().accessDeniedPage("/accessDenied");// 86400
		 http.logout().logoutUrl("/logout").logoutSuccessUrl("/login?logout").invalidateHttpSession(true).permitAll()
		.clearAuthentication(true).deleteCookies("JSESSIONID");
		
		/* Added for Concurrent Session Management*/
	/*	http.sessionManagement()
		.sessionFixation().changeSessionId()
		.maximumSessions(1).maxSessionsPreventsLogin(true).expiredUrl("/login?error")
		.sessionRegistry(this.sessionRegistry()).and().and().csrf();*/
		
		/* Added for CSRF removing token from headers or protecting from xss attacks*/
		http.headers().cacheControl().and().contentTypeOptions().and().frameOptions().sameOrigin()
		.httpStrictTransportSecurity().and().xssProtection().xssProtectionEnabled(true);
		http.requiresChannel().antMatchers("/paymentResponse").requiresInsecure().and().csrf().disable();
		http.requiresChannel().antMatchers("/paymentResponseAfterS2s").requiresInsecure().and().csrf().disable();
		
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
		authenticationProvider.setUserDetailsService(userDetailsService);
		authenticationProvider.setPasswordEncoder(passwordEncoder());
		return authenticationProvider;
	}

/*	@Bean
	public PersistentTokenBasedRememberMeServices getPersistentTokenBasedRememberMeServices() {
		PersistentTokenBasedRememberMeServices tokenBasedservice = new PersistentTokenBasedRememberMeServices(
				"remember-me", userDetailsService, tokenRepository);
		return tokenBasedservice;
	}*/

	@Bean
	public AuthenticationTrustResolver getAuthenticationTrustResolver() {
		return new AuthenticationTrustResolverImpl();
	}

	/* To allow Pre-flight [OPTIONS] request from browser */
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
	}

	@Bean
	public SessionRegistry sessionRegistry() {
		return new SessionRegistryImpl();
	}


	@Bean
	public MessageSource messageSource() {

		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasenames("messages");
		/* if true, the key of the message will be displayed if the key is not
		 found, instead of throwing a NoSuchMessageException*/
		messageSource.setUseCodeAsDefaultMessage(true);
		messageSource.setDefaultEncoding("UTF-8");
		// # -1 : never reload, 0 always reload
		messageSource.setCacheSeconds(0);
		return messageSource;

	}

	/**
	 * Enabling the concurrent session-control support is to add the following
	 * listener
	 * 
	 * @return
	 */
	@Bean
	public HttpSessionEventPublisher httpSessionEventPublisher() {
		return new HttpSessionEventPublisher();
	}
	
	@Bean
	public UsernamePasswordAuthenticationFilter authenticationFilter() {
	    CustomUsernamePasswordAuthenticationFilter authFilter = new CustomUsernamePasswordAuthenticationFilter();
	    authFilter.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/login","POST"));
	    List<AuthenticationProvider> authenticationProviderList = new ArrayList<AuthenticationProvider>();
	    authenticationProviderList.add(authenticationProvider);
	    AuthenticationManager authenticationManager = new ProviderManager(authenticationProviderList);
	    authFilter.setAuthenticationManager(authenticationManager);
	    authFilter.setUsernameParameter("ssoId");
	    authFilter.setPasswordParameter("password");
	    return authFilter;
	}
	
	 @Bean
	   public CompositeSessionAuthenticationStrategy concurrentSession() {

	            ConcurrentSessionControlAuthenticationStrategy concurrentAuthenticationStrategy = new ConcurrentSessionControlAuthenticationStrategy(sessionRegistry());
	            concurrentAuthenticationStrategy.setMaximumSessions(1);
	            concurrentAuthenticationStrategy.setExceptionIfMaximumExceeded(true);
	            List<SessionAuthenticationStrategy> delegateStrategies = new ArrayList<SessionAuthenticationStrategy>();
	            delegateStrategies.add(concurrentAuthenticationStrategy);
	            delegateStrategies.add(new SessionFixationProtectionStrategy());
	            delegateStrategies.add(new RegisterSessionAuthenticationStrategy(sessionRegistry()));
	            CompositeSessionAuthenticationStrategy authenticationStrategy =  new CompositeSessionAuthenticationStrategy(delegateStrategies);
	            return authenticationStrategy;
	    }

	   
}
