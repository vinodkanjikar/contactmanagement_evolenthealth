 package com.evolent.health.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author VinodK
 *
 */
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer{

	/**
	 * 
	 */
	public SecurityInitializer() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * Enabling the concurrent session-control support is to add the following
	 * listener
	 * 
	 * @return boolean
	 */
	@Override
	protected boolean enableHttpSessionEventPublisher() {
		return true;
	}
}
