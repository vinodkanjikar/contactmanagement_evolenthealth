package com.evolent.health.security;

import java.io.IOException;
import java.text.MessageFormat;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.evolent.health.entity.LoginDetail;
import com.evolent.health.service.LoginService;
import com.evolent.health.utility.MasterUtility;



/**
 * @author VinodK
 *
 */
public class CustomUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	
	private static final String LOGIN_SUCCESS_URL = "/login"; 
	private static final String LOGIN_ERROR_URL = "/login?error";
	private static final int MAX_ATTEMPTS = 5;
	private String captchaString;
	
	@Autowired
	MasterUtility masterUtility;
	
	@Autowired
	LoginService loginService;
	CustomUsernamePasswordAuthenticationFilter(){
		
	}
	
	@Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
       String captcha = request.getParameter("txtInput");
       String captcha2=request.getParameter("captcha");
        
        System.out.println("Entered captcha value--"+captcha);
        System.out.println("Actual captcha value--"+captcha2);
        if(null != captcha && !"".equals(captcha)){
        	try {
				masterUtility.checkCaptcha(captcha,request.getRemoteAddr());
			} catch (Exception e) {
				throw new UsernameNotFoundException("Captcha not found or invalid captcha");
			}
        } 
        return super.attemptAuthentication(request, response); 
    }

    
    
    @Override
    protected String obtainUsername(HttpServletRequest request)
    {
        String username = request.getParameter(getUsernameParameter());
        System.out.println("UserName :="+username);
        String extraInput = request.getParameter("txtInput");
        System.out.println("Captch=="+extraInput);
        String combinedUsername = username + ":" + extraInput;
        return combinedUsername;
    }
    
    
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain , Authentication authResult) throws IOException, ServletException {

    	captchaString=request.getParameter("txtInput");
    	String username = request.getParameter(getUsernameParameter());
        String contextPath = request.getContextPath();
        String url = MessageFormat.format(LOGIN_SUCCESS_URL, contextPath, captchaString);
        setAuthenticationSuccessHandler(new SimpleUrlAuthenticationSuccessHandler(url));
        loginService.resetFailAttempts(username);
        super.successfulAuthentication(request, response, chain, authResult);

    }
    
    
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {

        String contextPath = request.getContextPath();
        String url = MessageFormat.format(LOGIN_ERROR_URL, contextPath, captchaString);
        setAuthenticationFailureHandler(new SimpleUrlAuthenticationFailureHandler(url));
        String username = request.getParameter(getUsernameParameter());
        LoginDetail userAttempt = loginService.getUserAttempts(username);
		 int attempts = 0;
		if(userAttempt !=null)
		{
			
			    if(username !=null )
			    {    
			    	int count= userAttempt.getInvalidLoginAttempt();
			    		if(count<MAX_ATTEMPTS)
			    			loginService.updateFailAttempts(username);
			    		else
			    		{
			    			loginService.resetBlockedAccount(username);
			    			throw new LockedException("User Account is locked!System generated password mail has been sent on registered E-mail ID. Please login with the password provided and change the password.");
			    		}
				 }
			    else
			    {
			    	attempts = userAttempt.getInvalidLoginAttempt();
			        loginService.updateFailAttempts(username);  
			        if (attempts + 1 >= MAX_ATTEMPTS) 
			        {                 
			        	loginService.resetBlockedAccount(username); 
			        }                                   
			      }
		}
        super.unsuccessfulAuthentication(request, response, failed);
    }
   
    public String getCaptchaString() {
        return captchaString;
    }

    public void setCaptchaString(String corporateId) {
        this.captchaString = corporateId;
    }
}
