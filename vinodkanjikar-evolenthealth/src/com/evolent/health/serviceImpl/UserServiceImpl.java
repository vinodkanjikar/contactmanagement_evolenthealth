package com.evolent.health.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.evolent.health.dao.UserDAO;
import com.evolent.health.entity.LoginDetail;
import com.evolent.health.service.UserService;



/**
 * @author VinodK
 *
 */
@Service
@Transactional
public class UserServiceImpl implements UserService{

		
	@Autowired
	private UserDAO dao1;

 	public LoginDetail findBySSO(String sso, Integer authStatus) {
		LoginDetail user = dao1.findBySSO(sso);
		return user;
	}
}
