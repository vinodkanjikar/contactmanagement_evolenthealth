/**
 * 
 */
package com.evolent.health.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.evolent.health.dao.RegistrationDao;
import com.evolent.health.entity.ContactDetails;
import com.evolent.health.service.RegistrationService;
import com.evolent.health.ws.ContactDetailsDTO;

/**
 * @author VinodK
 *
 */
@Service
@Transactional
public class RegistrationSeriveImpl implements RegistrationService{

	 
	@Autowired
	RegistrationDao registrationDao;

	@Override
	public List<ContactDetails> addContactDetails(ContactDetailsDTO detailsDTO) {
		return registrationDao.addContactDetails(detailsDTO);
	}

	@Override
	public List<ContactDetails> getContactDetails(ContactDetailsDTO detailsDTO) {
		return registrationDao.getContactDetails(detailsDTO);
	}

	@Override
	public List<ContactDetails> deleteContactDetails(ContactDetailsDTO detailsDTO) {
		return registrationDao.deleteContactDetails(detailsDTO);
	}

	@Override
	public ContactDetails editContactDetails(ContactDetailsDTO detailsDTO) {
		return registrationDao.editContactDetails(detailsDTO);
	}

	 

 }
