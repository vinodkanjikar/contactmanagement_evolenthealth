package com.evolent.health.serviceImpl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.evolent.health.dao.LoginDAO;
import com.evolent.health.entity.LoginDetail;
import com.evolent.health.service.LoginService;


/**
 * @author VinodK
 *
 */
@Service("loginService")
@Transactional
public class LoginServiceImpl implements LoginService{

	@Autowired
	private LoginDAO dao;

	public LoginDetail findBySSO(String sso) {
		LoginDetail user = dao.findBySSO(sso);
		return user;
	}

	@Override
	public LoginDetail getUserDetails(String userName) {
		LoginDetail user = dao.getUserDetails(userName);
		return user;
	}

	@Override
	public void updateLoggingStatus(int gudUserId, int status) {
		dao.updateLoggingStatus(gudUserId, status);
		
	}
	
	@Override
	public void updateFailAttempts(String username) {
		dao.updateFailAttempts(username);
		
	}

	@Override
	public void resetFailAttempts(String username) {
		dao.resetFailAttempts(username);	
		
	}

	@Override
	public LoginDetail getUserAttempts(String username) {
		return dao.getUserAttempts(username);
		
	}

	@Override
	public void resetBlockedAccount(String username) {
		 dao.resetBlockedAccount(username);
		
	}

	@Override
	public String insertCaptcha(String captcha,String ip) {
		return dao.insertCaptcha(captcha,ip);
	}

}
