package com.evolent.health.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.evolent.health.appconstant.AppConstants;
import com.evolent.health.entity.ContactDetails;
import com.evolent.health.entity.LoginDetail;
import com.evolent.health.service.RegistrationService;
import com.evolent.health.utility.MasterUtility;
import com.evolent.health.ws.AjaxResponse;
import com.evolent.health.ws.ContactDetailsDTO; 

/**
 * @author VinodK
 *
 */
@RestController
@RequestMapping(value="/rest/")
public class ResController extends AppConstants{
	private static final Logger logger = Logger.getLogger(ResController.class);  
	private static final String THIS_COMPONENT = ResController.class.getName() + SPACE + COLON + SPACE;

	@Autowired
	RegistrationService registrationService;

	@Autowired
	MasterUtility masterUtility;
	
	/**
	 * Rest WS to add contact information
	 * @param detailsDTO
	 * @param request
	 * @return
	 */
	@RequestMapping(value="addContactDetail" , method=RequestMethod.POST,headers="Accept=application/json")
	public AjaxResponse<Object> addContactDetail (@RequestBody ContactDetailsDTO detailsDTO,HttpServletRequest request) { 
		logger.info(THIS_COMPONENT+"RegistrationController : addContactDetail() Start::"); 
		AjaxResponse<Object> ajax =new AjaxResponse<Object>();
		try {
			LoginDetail loggedInUser = (LoginDetail) request.getSession().getAttribute("LogInUser");	
			if(null !=loggedInUser)
			{ 
				detailsDTO.setCreatedBy(loggedInUser.getLoginDetailsId());
				List<ContactDetails> detailsList=registrationService.addContactDetails(detailsDTO);
				ajax.setStatus(1);
				if(detailsDTO.isFailure())
				{
					ajax.setMessage(detailsDTO.getMessage());
				}
				else if(detailsDTO.getId()!=null && detailsDTO.getId()>0)
					ajax.setMessage("Record updated successfuly.");
				else
					ajax.setMessage("Record save successfuly.");
				ajax.setData(detailsList);
			}
			else
			{
				ajax.setStatus(0);
				ajax.setMessage("Un-Authorize Access");
			}
			
		} catch (Exception e) {
			logger.error(THIS_COMPONENT+"RegistrationController : addContactDetail() error::"+e.getMessage());
			e.printStackTrace();
			ajax.setStatus(0);
			ajax.setMessage("Please contact administrator.");;
		}
		logger.info(THIS_COMPONENT+"RegistrationController : addContactDetail() End::");
		return ajax;
	}
	
	/**
	 * 	 * Rest WS to edit contact information
	 * @param detailsDTO
	 * @param request
	 * @return
	 */
	@RequestMapping(value="editContactDetail" , method=RequestMethod.POST,headers="Accept=application/json")
	public AjaxResponse<Object> editContactDetail (@RequestBody ContactDetailsDTO detailsDTO,HttpServletRequest request) { 
		logger.info(THIS_COMPONENT+"RegistrationController : editContactDetail() Start::"); 
		AjaxResponse<Object> ajax =new AjaxResponse<Object>();
		try {
			LoginDetail loggedInUser = (LoginDetail) request.getSession().getAttribute("LogInUser");	
			if(null !=loggedInUser)
			{ 
				detailsDTO.setCreatedBy(loggedInUser.getLoginDetailsId());
				ContactDetails detail=registrationService.editContactDetails(detailsDTO);
				ajax.setStatus(1);
				ajax.setData(detail);
			}
			else
			{
				ajax.setStatus(0);
				ajax.setMessage("Un-Authorize Access");
			}
			
		} catch (Exception e) {
			logger.error(THIS_COMPONENT+"RegistrationController : editContactDetail() error::"+e.getMessage());
			e.printStackTrace();
			ajax.setStatus(0);
			ajax.setMessage("Please contact administrator.");;
		}
		logger.info(THIS_COMPONENT+"RegistrationController : editContactDetail() End::");
		return ajax;
	}
	
	/**
	 * Rest WS to delete contact information
	 * @param detailsDTO
	 * @param request
	 * @return
	 */
	@RequestMapping(value="deleteContactDetail" , method=RequestMethod.POST,headers="Accept=application/json")
	public AjaxResponse<Object> deleteContactDetail (@RequestBody ContactDetailsDTO detailsDTO,HttpServletRequest request) { 
		logger.info(THIS_COMPONENT+"RegistrationController : deleteContactDetail() Start::"); 
		AjaxResponse<Object> ajax =new AjaxResponse<Object>();
		try {
			LoginDetail loggedInUser = (LoginDetail) request.getSession().getAttribute("LogInUser");	
			if(null !=loggedInUser)
			{ 
				detailsDTO.setCreatedBy(loggedInUser.getLoginDetailsId());
				List<ContactDetails> detail=registrationService.deleteContactDetails(detailsDTO);
				ajax.setStatus(1);
				ajax.setData(detail);
			}
			else
			{
				ajax.setStatus(0);
				ajax.setMessage("Un-Authorize Access");
			}
			
		} catch (Exception e) {
			logger.error(THIS_COMPONENT+"RegistrationController : deleteContactDetail() error::"+e.getMessage());
			e.printStackTrace();
			ajax.setStatus(0);
			ajax.setMessage("Please contact administrator.");;
		}
		logger.info(THIS_COMPONENT+"RegistrationController : deleteContactDetail() End::");
		return ajax;
	}
}
