package com.evolent.health.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.evolent.health.appconstant.AppConstants;
import com.evolent.health.configuration.LoggingInterceptor;
import com.evolent.health.entity.LoginDetail;
import com.evolent.health.entity.RequestMsgLog;
import com.evolent.health.service.LoginService;
import com.evolent.health.service.RegistrationService;
import com.evolent.health.service.UserService;
import com.evolent.health.utility.MasterUtility;
import com.evolent.health.ws.ContactDetailsDTO;

/**
 * @author VinodK
 *
 */
@Controller
public class LoginController extends AppConstants {

	private static final Logger logger = Logger.getLogger(LoginController.class);  
	private static final String THIS_COMPONENT = LoginController.class.getName() + SPACE + COLON + SPACE;

	@Autowired
	LoginService loginService;
	@Autowired
	UserService userService; 

	@Autowired
	MasterUtility masterUtility;
	
	@Autowired
	RegistrationService registrationService;

	@Autowired
	AuthenticationTrustResolver authenticationTrustResolver;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String homePage(ModelMap model,HttpServletRequest request, HttpServletResponse response) throws IOException 
	{
		return "redirect:login";
	}
	/**
	 * This method handles login GET requests. If users is already logged-in and
	 * tries to goto login page again, will be redirected to list page.
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage(HttpServletRequest request,ModelMap model) 
	{

		logger.info(THIS_COMPONENT + "Inside login Method:");
		if (isCurrentAuthenticationAnonymous()) 
		{		logger.info(THIS_COMPONENT + "Inside IF:");
		return "index";
		} else {
			return "redirect:/home";
		}
	}

	/**
	 * @param model
	 * @param request
	 * @param session
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String goToHome(ModelMap model,HttpServletRequest request,HttpSession session,HttpServletResponse response) 
	{	
		logger.info(THIS_COMPONENT + "Inside home Method:");
		LoginDetail user = null;
		if (null != getPrincipal()) 
		{
			user = new LoginDetail();
			logger.info(THIS_COMPONENT + "Inside IF Method::");
			user = getPrincipal();
			request.getSession().setAttribute("LogInUser", user);			 
			try 
			{				 
				if(user.getLoggedInStatus()!=1)
				{
					logger.info(THIS_COMPONENT + "Successful login:");
					loginService.updateLoggingStatus(user.getLoginDetailsId(),1);
				}
				RequestMsgLog uRLAuditTrail= new LoggingInterceptor().auditTrail(request, response);
				if(null !=uRLAuditTrail)
				{
					masterUtility.auditTrailSave(uRLAuditTrail);
				}
				model.addAttribute("contactDto",new ContactDetailsDTO());
				model.addAttribute("contactList",registrationService.getContactDetails(null));
				return "dashboard";
			}
			catch (Exception e)
			{
				logger.error(THIS_COMPONENT + "Exception Occurred InsideLoginController:goToHome():", e);
			}

		}
		else{
			return "redirect:/login";
		}
		return "redirect:/dashboard";
	}

	/**
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public String dashboard(Model model, HttpServletRequest request)

	{
		logger.info(THIS_COMPONENT + "dashboard Called: This is info Logger:");
		LoginDetail loggedInUser = (LoginDetail) request.getSession().getAttribute("LogInUser");	
		if(null !=loggedInUser)
		{ 
			return "dashboard";

		}
		else
		{
			return "redirect:/login";
		}
	}

	 
	/**
	 * User check 
	 * @return
	 */
	private LoginDetail getPrincipal()
	{
		logger.info(THIS_COMPONENT + "Inside getPrincipal() Method:");
		String gutPan = null;
		LoginDetail user = new LoginDetail();
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof UserDetails) 
		{
			logger.info(THIS_COMPONENT + "Inside getPrincipal() Method IF Block:");
			gutPan = ((UserDetails)principal).getUsername();
			user = loginService.getUserDetails(gutPan);
			if(user==null)
			{
				System.out.println("Invalid User ID:");
				logger.info(THIS_COMPONENT + "Invalid Request:");
			}
		}
		else {
			logger.info(THIS_COMPONENT + "Inside getPrincipal() Method Else Block:");
			gutPan = principal.toString();
		}
		return user;
	}

	/**
	 * This method handles logout requests. Toggle the handlers if you are
	 * RememberMe functionality is useless in your app.
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) 
	{
		LoginDetail loggedInUser = (LoginDetail) request.getSession().getAttribute("LogInUser");
		logger.info(THIS_COMPONENT + "Inside logout Method:");
		loginService.updateLoggingStatus(loggedInUser.getLoginDetailsId(), 0);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			logger.info(THIS_COMPONENT + "Inside IF Method:");
			new SecurityContextLogoutHandler().logout(request, response, auth);
			SecurityContextHolder.getContext().setAuthentication(null);
		}
		return "redirect:/login?logout";
	}

	 
	/**
	 * This method returns true if users is already authenticated [logged-in],
	 * else false.
	 */
	private boolean isCurrentAuthenticationAnonymous() {
		logger.info(THIS_COMPONENT + "Inside isCurrentAuthenticationAnonymous Method:");
		if(SecurityContextHolder.getContext().getAuthentication() == null)
		{
			logger.info(THIS_COMPONENT + "Inside IF Method:");
			return true;
		}else{
			logger.info(THIS_COMPONENT + "Inside Else Method:");
			final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			return authenticationTrustResolver.isAnonymous(authentication);
		}
	}

	/**
	 * Method added to get system details.
	 * 
	 * @param request
	 * @return
	 */
	public String getSystemDetails(HttpServletRequest request){

		String sysInfo = "";
		sysInfo = request.getRemoteAddr();

		String userAgent=(String)request.getHeader("User-Agent"); 
		String browserName = null,browserVer = null;
		if(userAgent.contains("Chrome")){ //checking if Chrome
			String substring=userAgent.substring(userAgent.indexOf("Chrome")).split(" ")[0];
			browserName=substring.split("/")[0];
			browserVer=substring.split("/")[1];
			sysInfo = sysInfo +" , "+ browserName+" , "+browserVer;
		}
		else if(userAgent.contains("Firefox")){  //Checking if Firefox
			String substring=userAgent.substring(userAgent.indexOf("Firefox")).split(" ")[0];
			browserName=substring.split("/")[0];
			browserVer=substring.split("/")[1];
			sysInfo = sysInfo +" , "+ browserName+" , "+browserVer;
		}
		else if(userAgent.contains("MSIE")){ //Checking if Internet Explorer
			String substring=userAgent.substring(userAgent.indexOf("MSIE")).split(";")[0];
			browserName=substring.split(" ")[0];
			browserVer=substring.split(" ")[1];
			sysInfo = sysInfo +" , "+ browserName+" , "+browserVer;
		}
		else if(userAgent.contains("rv")){ //checking if Internet Explorer 11
			String substring=userAgent.substring(userAgent.indexOf("rv"),userAgent.indexOf(")"));
			browserName="IE";
			browserVer=substring.split(":")[1];
			sysInfo = sysInfo +" , "+ browserName+browserVer;
		}


		return sysInfo;
	}

	 
	/**
	 * Method added to insert capcha in respective table for captcha validation.
	 * @param request
	 * @param response
	 * @param model
	 * @param captcha
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "info/insertCaptcha", method = RequestMethod.GET)
	public @ResponseBody String insertCaptcha(HttpServletRequest request, HttpServletResponse response,ModelMap model
			,@RequestParam("captcha") String captcha) throws Exception {

		logger.info(THIS_COMPONENT + "LoginController : insertCaptcha() Start:");
		try{
			logger.info("captcha"+captcha);
			loginService.insertCaptcha(captcha,request.getRemoteAddr());
			return "success";
		}catch(Exception e){
			logger.error(THIS_COMPONENT + "Exception Occurred Inside insertCaptcha():", e);
			return "fail";
		}
	}

	/**
	 * @throws IOException
	 * 
	 * Method added to truncate captcha table.
	 */
	@Scheduled(cron="0 59 23 * * ?") //12 am (midnight)
	public void clearCaptchaDetails() throws IOException {
		logger.info("Inside loginController : clearCaptchaDetails() method");
		try{
			masterUtility.clearCaptchaDetails();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.error(THIS_COMPONENT + "Exception Occurred Inside LoginController:clearCaptchaDetails():", ex);
		} 

	}

}

