package com.evolent.health.ws;

public class AjaxResponse<T> {
	private T data;
	private int status = 0;
	private String message = "";
	private String url="";
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public AjaxResponse(T data) {
		this.data = data;
	}

	public AjaxResponse() {

	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	

}
