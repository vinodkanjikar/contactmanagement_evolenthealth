package com.evolent.health.ws;

public class AjaxRequest<T> {
	private T data;
	  
	public AjaxRequest(T data) {
		this.data = data;
	}

	public AjaxRequest() {

	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	
 

	

}
