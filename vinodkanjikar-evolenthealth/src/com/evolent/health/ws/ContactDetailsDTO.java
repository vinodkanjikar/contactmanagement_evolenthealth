package com.evolent.health.ws;

public class ContactDetailsDTO {
	private String contactNumber;
 	private String emailId;
	private String firstName;
 	private String lastName;
	private String middleName;
	private Integer createdBy;
	private Integer id;
	private Integer optVersion;
	private boolean failure;
	private String message;
	
	public boolean isFailure() {
		return failure;
	}
	public void setFailure(boolean failure) {
		this.failure = failure;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getOptVersion() {
		return optVersion;
	}
	public void setOptVersion(Integer optVersion) {
		this.optVersion = optVersion;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	@Override
	public String toString() {
		return "RegDetailsDTO [contactNumber=" + contactNumber + ", emailId=" + emailId + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", middleName=" + middleName + "]";
	}
 	 
	  	 
}
