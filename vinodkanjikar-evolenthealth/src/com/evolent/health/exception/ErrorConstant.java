/**
 * 
 */
package com.evolent.health.exception;

/**
 * @author VinodK
 *
 *This class contains application error constants.
 */
public class ErrorConstant {

	public static final String DATABASE_GENERIC_ERROR = "DB0001";

}
