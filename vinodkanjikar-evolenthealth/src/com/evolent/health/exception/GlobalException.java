/**
 * 
 */
package com.evolent.health.exception;

/**
 * @author VinodK
 * 
 * This class is for throwing user defined exception.
 *
 */
public class GlobalException extends Exception{

	private static final long serialVersionUID = -9113791593351351641L;
	
	String errorCode;
	public GlobalException(String errorCode) {
		this.errorCode = errorCode;
    }
	
	public String toString(){ 
	       return ("Output String = "+errorCode) ;
	    }

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

}
