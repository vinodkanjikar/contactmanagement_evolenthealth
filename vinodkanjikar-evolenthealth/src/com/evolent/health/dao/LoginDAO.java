package com.evolent.health.dao;


import com.evolent.health.entity.LoginDetail;



/**
 * @author VinodK
 *
 */
public interface LoginDAO {
	
	public LoginDetail findBySSO(String sso);

	public LoginDetail getUserDetails(String userName);

	public void updateLoggingStatus(int userId, int status);
	
 	/* New added methods to restrict login attempts*/
	void updateFailAttempts(String username);
	
	void resetFailAttempts(String username);
	
	LoginDetail getUserAttempts(String username);
	
	void resetBlockedAccount(String username);
	
	public String insertCaptcha(String captcha,String ip);
	
}
