package com.evolent.health.dao;

import com.evolent.health.entity.LoginDetail;

 
/**
 * @author VinodK
 *
 */
public interface UserDAO {
 	LoginDetail findBySSO(String sso);
 }
