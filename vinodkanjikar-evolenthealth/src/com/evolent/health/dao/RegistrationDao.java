/**
 * 
 */
package com.evolent.health.dao;

import java.util.List;

import com.evolent.health.entity.ContactDetails;
import com.evolent.health.ws.ContactDetailsDTO;

/**
 * @author VinodK
 *
 */
public interface RegistrationDao { 
	List<ContactDetails> addContactDetails(ContactDetailsDTO detailsDTO);
	List<ContactDetails> getContactDetails(ContactDetailsDTO detailsDTO);
	List<ContactDetails> deleteContactDetails(ContactDetailsDTO detailsDTO);
	ContactDetails editContactDetails(ContactDetailsDTO detailsDTO);

}
