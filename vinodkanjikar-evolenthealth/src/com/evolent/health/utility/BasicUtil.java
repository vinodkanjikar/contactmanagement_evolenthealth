package com.evolent.health.utility;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.evolent.health.appconstant.AppConstants;



/**
 * @author VinodK
 *
 */
public class BasicUtil  extends AppConstants
{

	private static final Logger logger = Logger.getLogger(BasicUtil.class);
	private static final String THIS_COMPONENT = BasicUtil.class.getName() + SPACE + COLON + SPACE;
	
	public static final SimpleDateFormat dateformatter = new SimpleDateFormat(
			"dd/MM/yyyy");

	public static String generatePassword() {

		int noOfCAPSAlpha = 1;
		int noOfDigits = 1;
		int noOfSplChars = 1;
		int minLen = 8;
		int maxLen = 12;

		char[] pswd = RandomPasswordGenerator.generatePswd(minLen, maxLen,
				noOfCAPSAlpha, noOfDigits, noOfSplChars);
		logger.info("auto generated password Len = " + pswd.length + ", "
				+ new String(pswd));

		System.out
				.println("From generatePassword() of BasicUtil ====================>");
		return new String(pswd);
	}
	
	
	/**
	 * Method added by Sandesh to get current Time Stamp
	 * 
	 * @return
	 */
	public static Timestamp getCurrentTimestamp() 
	{
		//logger.info("From BasicUtil getCurrentTimestamp() start:");
		logger.info(THIS_COMPONENT + "From BasicUtil getCurrentTimestamp() start:");
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Timestamp timestamp = null;
		try 
		{
			logger.info(THIS_COMPONENT + "Inside Try Block:");
			timestamp = new Timestamp(df.parse(df.format(new Date())).getTime());
		} catch (Exception e) 
		{
			logger.error(THIS_COMPONENT + "Inside Catch Block Exception Error:", e);
			//logger.error("BasicUtil getCurrentTimestamp() error:" + e);
		}
	//	logger.info("From BasicUtil getCurrentTimestamp() end:");
		logger.info(THIS_COMPONENT + "From BasicUtil getCurrentTimestamp() end::");
		return timestamp;
	}
	/**
	 * Method added by Sandesh to read property value based on property name.
	 * 
	 * @param propName
	 * @return String
	 * @throws IOException
	 */
	public String getPropertyValues(String propName) throws IOException 
	{
		//logger.info("BasicUtil  getPropertyValues(String propName) start:");
		logger.info(THIS_COMPONENT + "BasicUtil  getPropertyValues(String propName) start:");
		Properties prop = new Properties();
		InputStream inputStream = null;
		// loading all Values from application.properties
		inputStream = getClass().getClassLoader().getResourceAsStream("application.properties");
		if (null != inputStream) 
		{
			logger.info(THIS_COMPONENT + "BasicUtil  getPropertyValues(String propName) IF Block:");
			prop.load(inputStream);
		} else {
			//logger.error("EkycUtility  getPropertyValues(String propName) not found in the classpath:");
			logger.info(THIS_COMPONENT + "BasicUtil  getPropertyValues(String propName) Else Block:");
			throw new FileNotFoundException("property file not found in the classpath");
		}
		logger.info(THIS_COMPONENT + "BasicUtil  getPropertyValues(String propName) end:");
		//logger.info("BasicUtil  getPropertyValues(String propName) end:");
		return prop.getProperty(propName);
	}
	/**
	 * Method added by Sandesh to format DateString To Date.
	 * 
	 * @param request
	 * @return String
	 */
	public static Date formateDateStringToDate(String dateString) 
	{
		logger.info(THIS_COMPONENT + "From BasicUtil formateDateStringToDate() start::");
		//logger.info("From BasicUtil formateDateStringToDate() start:");
		if (null != dateString && !dateString.equals("")) 
		{
			logger.info(THIS_COMPONENT + "From BasicUtil formateDateStringToDate() If Block::");
			if (dateString.charAt(1) == '/') {
				dateString = "0" + dateString;
			}
			Date date = null;
			try {
				logger.info(THIS_COMPONENT + "From BasicUtil formateDateStringToDate() Try Block::");
				date = dateformatter.parse(dateString);
			} catch (Exception e) 
			{
				logger.error(THIS_COMPONENT + "Inside Catch Block Exception Error:", e);
				//logger.error("BasicUtil formateDateStringToDate() error:"+ e.getMessage());
			}
			return date;
		} else {
			return null;
		}
	}
}
