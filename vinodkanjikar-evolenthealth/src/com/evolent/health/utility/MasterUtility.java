package com.evolent.health.utility;



import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.evolent.health.entity.Captcha;
import com.evolent.health.entity.RequestMsgLog;
import com.evolent.health.exception.ErrorConstant;
import com.evolent.health.exception.GlobalException;
 

/**
 * @author VinodK
 *
 */
@Transactional
@Repository
public class MasterUtility extends HibernateDaoSupport 
{
	
	@Autowired
	public void getHibernateSession(SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);
	}

	public static final String COLON = ":";
    public static final String SPACE = "";
	private static final Logger logger = Logger.getLogger(MasterUtility.class);
	private static final String THIS_COMPONENT = MasterUtility.class.getName() + SPACE + COLON + SPACE;
   
  	 /**
  	 * @param auditTrailInfo
  	 * Added by Swati to store info into RequestMsgLog 
  	 */
  	@Transactional
     public void auditTrailSave(RequestMsgLog auditTrailInfo){    
  		logger.info("MasterUtility auditTrailSave(AuditTrailPage auditTrailInfo) start:");
             try{
            	 logger.info("MasterUtility auditTrailSave(AuditTrailPage auditTrailInfo) try block:");  
           	  getHibernateTemplate().save(auditTrailInfo);
              }
             catch(Exception e){
            	 logger.error("Exception Occurred at MasterUtility :auditTrailSave(AuditTrailPage auditTrailInfo): ",e);
             }
      }

 
  	@SuppressWarnings("unchecked")
	public  String checkCaptcha(String captcha, String ip) throws Exception
	{
		logger.info(THIS_COMPONENT + "MasterUtility checkCaptcha() Start::");
		try
		{
			Query query = (Query) getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from Captcha where captcha =:captcha and ip=:ip");
			query.setParameter("captcha", captcha);
			query.setParameter("ip", ip);
			List<Captcha> captchaList = query.list();  
			
			if (null != captchaList && captchaList.size() <= 0) {
				throw new Exception();
			}
		}catch(Exception e)
		{
			logger.error(THIS_COMPONENT + "Exception in class MasterUtility : checkCaptcha() method :", e);
			throw new GlobalException(ErrorConstant.DATABASE_GENERIC_ERROR);
		}
		return "success";
	}
  	
  	
  	/**
  	 * @throws IOException
  	 * 
  	 * Method added by Sandesh to truncate captcha table.
  	 */
  	public void clearCaptchaDetails() throws IOException {
  		logger.info("Inside MasterUtility : clearCaptchaDetails() method");
  		try{
  			getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery("truncate table captcha_dtl").executeUpdate();
  		}
  		catch(Exception ex)
  		{
  			ex.printStackTrace();
  			logger.error("Exception Occurred Inside MasterUtility:clearCaptchaDetails():", ex);
  		} 

  	}

}




 