package com.evolent.health.utility;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.evolent.health.appconstant.AppConstants;

/**
 * @author VinodK
 *
 */
public class ProperFileUtility extends AppConstants
{
	//private static Logger log = Logger.getLogger(ProperFileUtility.class);
	
	//Added For Logger Functionality
			private static final Logger logger = Logger.getLogger(ProperFileUtility.class);
			private static final String THIS_COMPONENT = ProperFileUtility.class.getName() + SPACE + COLON + SPACE;
		
	
	
	// Reading File  From  Specified  Location
	public String getPropertyValues(String propName) throws IOException 
	{
		//log.info("properFileUtility  getPropertyValues(String propName) start:");
		logger.info(THIS_COMPONENT + "properFileUtility  getPropertyValues(String propName) start:");
		Properties prop = new Properties();
		InputStream inputStream = null;
		// loading all Values from application.properties
		inputStream = getClass().getClassLoader().getResourceAsStream("application.properties"); 
		if (null != inputStream  )
		{
			logger.info(THIS_COMPONENT + "properFileUtility  getPropertyValues(String propName) start If Block:");
			prop.load(inputStream); 
		} else 
		{
			logger.info(THIS_COMPONENT + "properFileUtility  getPropertyValues(String propName) not found in the classpath:");
			//log.error("properFileUtility  getPropertyValues(String propName) not found in the classpath:");
			throw new FileNotFoundException("property file not found in the classpath");
		}
		logger.info(THIS_COMPONENT + "properFileUtility  getPropertyValues(String propName) end:");
		//log.info("properFileUtility  getPropertyValues(String propName) end:");
		return prop.getProperty(propName);
	}
}
