package com.evolent.health.enumeration;

public enum EmailTypeEnum {
	REGISTRATION,
    RESET_FORGOT_PASSWORD,
    CHANGE_PASSWORD;

    public String subjectLine() {
        switch (this) {
            case REGISTRATION:
                return "Registration on E-Subscription portal";
            case RESET_FORGOT_PASSWORD:
                return "Request for password reset (Forgot Password)";
            case CHANGE_PASSWORD:
            	return "Confirmation for Change of Password.";
            default:
                return null;
        }
    }

}
