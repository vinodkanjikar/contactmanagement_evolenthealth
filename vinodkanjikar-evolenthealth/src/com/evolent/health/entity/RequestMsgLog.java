package com.evolent.health.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;


/**
 * The persistent class for the request_msg_log database table.
 * 
 */
@Entity
@Table(name="request_msg_log")
public class RequestMsgLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="rml_req_msg_id")
	private int reqMsgId;

	@Column(name="rml_browser_used")
	private String browserUsed;

	@Column(name="rml_browser_version")
	private String browserVersion;

	 
	@Column(name="rml_creation_date")
	private Date creationDate;

	@Column(name="rml_login_details_id")
	private int loginDetailsId;

	@Column(name="rml_os_info")
	private String osInfo;

	@Column(name="rml_os_version")
	private String osVersion;

	@Lob
	@Column(name="rml_req_action")
	private String reqAction;

	@Column(name="rml_req_ip_imei_no")
	private String reqIpImeiNo;

	@Column(name="rml_req_lat")
	private String reqLat;

	@Column(name="rml_req_long")
	private String reqLong;

	@Column(name="rml_req_type")
	private String reqType;

	public int getReqMsgId() {
		return reqMsgId;
	}

	public void setReqMsgId(int reqMsgId) {
		this.reqMsgId = reqMsgId;
	}

	public String getBrowserUsed() {
		return browserUsed;
	}

	public void setBrowserUsed(String browserUsed) {
		this.browserUsed = browserUsed;
	}

	public String getBrowserVersion() {
		return browserVersion;
	}

	public void setBrowserVersion(String browserVersion) {
		this.browserVersion = browserVersion;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public int getLoginDetailsId() {
		return loginDetailsId;
	}

	public void setLoginDetailsId(int loginDetailsId) {
		this.loginDetailsId = loginDetailsId;
	}

	public String getOsInfo() {
		return osInfo;
	}

	public void setOsInfo(String osInfo) {
		this.osInfo = osInfo;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public String getReqAction() {
		return reqAction;
	}

	public void setReqAction(String reqAction) {
		this.reqAction = reqAction;
	}

	public String getReqIpImeiNo() {
		return reqIpImeiNo;
	}

	public void setReqIpImeiNo(String reqIpImeiNo) {
		this.reqIpImeiNo = reqIpImeiNo;
	}

	public String getReqLat() {
		return reqLat;
	}

	public void setReqLat(String reqLat) {
		this.reqLat = reqLat;
	}

	public String getReqLong() {
		return reqLong;
	}

	public void setReqLong(String reqLong) {
		this.reqLong = reqLong;
	}

	public String getReqType() {
		return reqType;
	}

	public void setReqType(String reqType) {
		this.reqType = reqType;
	}
 

}