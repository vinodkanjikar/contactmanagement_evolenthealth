package com.evolent.health.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * @author VinodK
 *
 */
@Entity
@Table(name="mci_contact_dtls")
public class ContactDetails implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="contact_id")
	private int id;
	
	@Column(name="contact_tiltle")
	private String title;
	
	@Column(name="contact_first_name")
	private String firstName;
	
	@Column(name="contact_middle_name")
	private String middleName;
	
	@Column(name="contact_last_name")
	private String lastName;
	
	@Column(name="contact_mobile_country_code")
	private String mobileCountryCode;
	
	@Column(name="contact_mobile_number")
	private String mobileNumber;
	
	@Column(name="contact_email_id")
	private String emailId;
	
	@Column(name="contact_employee_id")
	private String employeeId;
	
	@Column(name="contact_designation")
	private String designation;
	
	@Column(name="created_by")
	private Integer createdBy;
	
	@Column(name="last_modified_by")
	private int modifiedBy;
	
	@Column(name="last_modification_date")
	private Date modificationDate;
	
	@Column(name="creation_date")
	private Date creationDate;
	
	@Column(name="contact_status")
	private int status;

	@Version
	@Column(name="opti_version")
	private int optiVersion;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobileCountryCode() {
		return mobileCountryCode;
	}

	public void setMobileCountryCode(String mobileCountryCode) {
		this.mobileCountryCode = mobileCountryCode;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public int getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(int modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getOptiVersion() {
		return optiVersion;
	}

	public void setOptiVersion(int optiVersion) {
		this.optiVersion = optiVersion;
	}
	
	
}
