package com.evolent.health.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the role_master database table.
 * 
 */
@Entity
@Table(name="role_master")
public class RoleMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="rm_role_id")
	private int roleId;

	@Column(name="rm_created_by")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="rm_creation_datetime")
	private Date creationDatetime;

	@Column(name="rm_is_active_flg")
	private String isActiveFlg;

	@Column(name="rm_modification_datetime")
	private Date modificationDatetime;

	@Column(name="rm_modified_by")
	private String modifiedBy;

	@Column(name="rm_role_name")
	private String roleName;

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDatetime() {
		return creationDatetime;
	}

	public void setCreationDatetime(Date creationDatetime) {
		this.creationDatetime = creationDatetime;
	}

	public String getIsActiveFlg() {
		return isActiveFlg;
	}

	public void setIsActiveFlg(String isActiveFlg) {
		this.isActiveFlg = isActiveFlg;
	}

	public Date getModificationDatetime() {
		return modificationDatetime;
	}

	public void setModificationDatetime(Date modificationDatetime) {
		this.modificationDatetime = modificationDatetime;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
}
 