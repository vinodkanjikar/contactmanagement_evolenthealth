package com.evolent.health.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the login_details database table.
 * 
 */
@Entity
@Table(name="login_details")
public class LoginDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ld_login_details_id")
	private int loginDetailsId;

	@Column(name="ld_contact_number")
	private String contactNumber;

	@Column(name="ld_created_by")
	private String createdBy;

	@Column(name="ld_creation_datetime")
	private Date creationDatetime;

	@Column(name="ld_email_id")
	private String emailId;

	@Column(name="ld_first_name")
	private String firstName;

	@Column(name="ld_invalid_login_attempt")
	private int invalidLoginAttempt;

	@Column(name="ld_is_active_flg")
	private String isActiveFlg;

	@Column(name="ld_is_force_change_password_flg")
	private String isForceChangePasswordFlg;

	@Column(name="ld_is_locked_flg")
	private String isLockedFlg;

	@Column(name="ld_last_change_password_datetime")
	private Date lastChangePasswordDatetime;

	@Column(name="ld_last_locked__datetime")
	private Date lastLockedDatetime;

	@Column(name="ld_last_logged_in__datetime")
	private Date lastLoggedInDatetime;

	@Column(name="ld_last_logged_out_datetime")
	private Date lastLoggedOutDatetime;

	@Column(name="ld_last_name")
	private String lastName;

	@Column(name="ld_middle_name")
	private String middleName;

	@Column(name="ld_modification_datetime")
	private Date modificationDatetime;

	@Column(name="ld_modified_by")
	private String modifiedBy;

	@Column(name="ld_password")
	private String password;

	@Column(name="ld_role_id")
	private int roleId;

	@Column(name="ld_service_end_date")
	private Date serviceEndDate;

	@Column(name="ld_service_start_date")
	private Date serviceStartDate;
	
	@Column(name="ld_logged_in_status")
	private int loggedInStatus;
	
	@Column(name="ld_is_profile_loaded")
	private String  isProfileLoaded;

	
	public String getIsProfileLoaded() {
		return isProfileLoaded;
	}

	public void setIsProfileLoaded(String isProfileLoaded) {
		this.isProfileLoaded = isProfileLoaded;
	}

	public int getLoggedInStatus() {
		return loggedInStatus;
	}

	public void setLoggedInStatus(int loggedInStatus) {
		this.loggedInStatus = loggedInStatus;
	}

	public int getLoginDetailsId() {
		return loginDetailsId;
	}

	public void setLoginDetailsId(int loginDetailsId) {
		this.loginDetailsId = loginDetailsId;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDatetime() {
		return creationDatetime;
	}

	public void setCreationDatetime(Date creationDatetime) {
		this.creationDatetime = creationDatetime;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public int getInvalidLoginAttempt() {
		return invalidLoginAttempt;
	}

	public void setInvalidLoginAttempt(int invalidLoginAttempt) {
		this.invalidLoginAttempt = invalidLoginAttempt;
	}

	public String getIsActiveFlg() {
		return isActiveFlg;
	}

	public void setIsActiveFlg(String isActiveFlg) {
		this.isActiveFlg = isActiveFlg;
	}

	public String getIsForceChangePasswordFlg() {
		return isForceChangePasswordFlg;
	}

	public void setIsForceChangePasswordFlg(String isForceChangePasswordFlg) {
		this.isForceChangePasswordFlg = isForceChangePasswordFlg;
	}

	public String getIsLockedFlg() {
		return isLockedFlg;
	}

	public void setIsLockedFlg(String isLockedFlg) {
		this.isLockedFlg = isLockedFlg;
	}

	public Date getLastChangePasswordDatetime() {
		return lastChangePasswordDatetime;
	}

	public void setLastChangePasswordDatetime(Date lastChangePasswordDatetime) {
		this.lastChangePasswordDatetime = lastChangePasswordDatetime;
	}

	public Date getLastLockedDatetime() {
		return lastLockedDatetime;
	}

	public void setLastLockedDatetime(Date lastLockedDatetime) {
		this.lastLockedDatetime = lastLockedDatetime;
	}

	public Date getLastLoggedInDatetime() {
		return lastLoggedInDatetime;
	}

	public void setLastLoggedInDatetime(Date lastLoggedInDatetime) {
		this.lastLoggedInDatetime = lastLoggedInDatetime;
	}

	public Date getLastLoggedOutDatetime() {
		return lastLoggedOutDatetime;
	}

	public void setLastLoggedOutDatetime(Date lastLoggedOutDatetime) {
		this.lastLoggedOutDatetime = lastLoggedOutDatetime;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Date getModificationDatetime() {
		return modificationDatetime;
	}

	public void setModificationDatetime(Date modificationDatetime) {
		this.modificationDatetime = modificationDatetime;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public Date getServiceEndDate() {
		return serviceEndDate;
	}

	public void setServiceEndDate(Date serviceEndDate) {
		this.serviceEndDate = serviceEndDate;
	}

	public Date getServiceStartDate() {
		return serviceStartDate;
	}

	public void setServiceStartDate(Date serviceStartDate) {
		this.serviceStartDate = serviceStartDate;
	}


}