/**
 * 
 */
package com.evolent.health.entity;




import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @author VinodK
 *
 */
@Entity
@Table(name="captcha_dtl")
public class Captcha implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cd_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer captchaId;

	@Column(name="cd_captcha")
	private String captcha;

	@Column(name="cd_ip")
	private String ip;

	public Integer getCaptchaId() {
		return captchaId;
	}

	public void setCaptchaId(Integer captchaId) {
		this.captchaId = captchaId;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

}