-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 29, 2020 at 09:01 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `contactmanagement`
--

-- --------------------------------------------------------

--
-- Table structure for table `captcha_dtl`
--

CREATE TABLE `captcha_dtl` (
  `cd_id` int(11) NOT NULL,
  `cd_captcha` varchar(50) NOT NULL,
  `cd_ip` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `captcha_dtl`
--

INSERT INTO `captcha_dtl` (`cd_id`, `cd_captcha`, `cd_ip`) VALUES
(1, 'e4xB5nt', '0:0:0:0:0:0:0:1'),
(2, 'z5jp8QN', '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `login_details`
--

CREATE TABLE `login_details` (
  `ld_login_details_id` int(11) NOT NULL,
  `ld_first_name` varchar(250) NOT NULL,
  `ld_middle_name` varchar(250) DEFAULT NULL,
  `ld_last_name` varchar(250) NOT NULL,
  `ld_contact_number` varchar(10) NOT NULL,
  `ld_email_id` varchar(320) NOT NULL,
  `ld_password` varchar(250) NOT NULL,
  `ld_service_start_date` datetime NOT NULL,
  `ld_service_end_date` datetime NOT NULL,
  `ld_invalid_login_attempt` int(11) DEFAULT NULL,
  `ld_role_id` int(11) NOT NULL,
  `ld_created_by` varchar(320) NOT NULL,
  `ld_creation_datetime` datetime NOT NULL,
  `ld_modified_by` varchar(320) DEFAULT NULL,
  `ld_modification_datetime` datetime DEFAULT NULL,
  `ld_is_locked_flg` char(1) NOT NULL,
  `ld_is_active_flg` char(1) NOT NULL,
  `ld_is_force_change_password_flg` char(1) NOT NULL,
  `ld_last_logged_in__datetime` datetime DEFAULT NULL,
  `ld_last_logged_out_datetime` datetime DEFAULT NULL,
  `ld_last_locked__datetime` datetime DEFAULT NULL,
  `ld_last_change_password_datetime` datetime DEFAULT NULL,
  `ld_logged_in_status` int(11) NOT NULL,
  `ld_is_profile_loaded` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_details`
--

INSERT INTO `login_details` (`ld_login_details_id`, `ld_first_name`, `ld_middle_name`, `ld_last_name`, `ld_contact_number`, `ld_email_id`, `ld_password`, `ld_service_start_date`, `ld_service_end_date`, `ld_invalid_login_attempt`, `ld_role_id`, `ld_created_by`, `ld_creation_datetime`, `ld_modified_by`, `ld_modification_datetime`, `ld_is_locked_flg`, `ld_is_active_flg`, `ld_is_force_change_password_flg`, `ld_last_logged_in__datetime`, `ld_last_logged_out_datetime`, `ld_last_locked__datetime`, `ld_last_change_password_datetime`, `ld_logged_in_status`, `ld_is_profile_loaded`) VALUES
(1, 'admin', NULL, 'admin', '9999999999', 'admin@gmail.com', '$2a$10$smjjGIvNtMvrXBxot8p9l.z1nbUPTJlCnIMUAwdL.57faZ/PukLrS', '2020-03-27 00:00:00', '2021-03-27 00:00:00', 0, 1, 'admin', '2020-03-27 00:00:00', NULL, NULL, 'N', 'A', 'N', '2020-03-27 21:48:05', NULL, NULL, NULL, 1, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `mci_contact_dtls`
--

CREATE TABLE `mci_contact_dtls` (
  `contact_id` int(11) NOT NULL,
  `contact_tiltle` varchar(10) DEFAULT NULL,
  `contact_first_name` varchar(250) NOT NULL,
  `contact_middle_name` varchar(250) DEFAULT NULL,
  `contact_last_name` varchar(250) NOT NULL,
  `contact_mobile_country_code` varchar(5) DEFAULT NULL,
  `contact_mobile_number` varchar(10) NOT NULL,
  `contact_email_id` varchar(254) NOT NULL,
  `contact_employee_id` varchar(10) DEFAULT NULL,
  `contact_designation` varchar(50) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `last_modified_by` int(11) DEFAULT NULL,
  `last_modification_date` timestamp NULL DEFAULT NULL,
  `contact_status` int(11) NOT NULL COMMENT '1 - Active 0 - Deactive',
  `opti_version` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mci_contact_dtls`
--

INSERT INTO `mci_contact_dtls` (`contact_id`, `contact_tiltle`, `contact_first_name`, `contact_middle_name`, `contact_last_name`, `contact_mobile_country_code`, `contact_mobile_number`, `contact_email_id`, `contact_employee_id`, `contact_designation`, `created_by`, `creation_date`, `last_modified_by`, `last_modification_date`, `contact_status`, `opti_version`) VALUES
(10, NULL, 'Vinod', '', 'Kanjikar', NULL, '9923400503', 'vinod.kanjikar@gmail.com', NULL, NULL, 1, '2020-03-28 15:01:56', 1, '2020-03-28 15:47:20', 1, 2),
(12, NULL, 'Vinod', '', 'K', NULL, '9999999999', 'vinodk@gmail.com', NULL, NULL, 1, '2020-03-28 15:07:08', 1, '2020-03-28 15:47:42', 1, 1),
(13, NULL, 'vvv', '', 'VV', NULL, '9999999888', 'vvvv@gmail.com', NULL, NULL, 1, '2020-03-28 16:00:40', 0, NULL, 0, 1),
(14, NULL, 'VK', '', 'KKK', NULL, '8999999999', 'kk90@gmail.com', NULL, NULL, 1, '2020-03-28 17:02:18', 0, NULL, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `request_msg_log`
--

CREATE TABLE `request_msg_log` (
  `rml_req_msg_id` int(11) NOT NULL,
  `rml_req_type` varchar(1) NOT NULL,
  `rml_req_action` text NOT NULL,
  `rml_login_details_id` int(11) DEFAULT NULL,
  `rml_creation_date` datetime NOT NULL,
  `rml_browser_used` varchar(50) DEFAULT NULL,
  `rml_browser_version` varchar(150) DEFAULT NULL,
  `rml_os_version` varchar(150) DEFAULT NULL,
  `rml_os_info` varchar(150) DEFAULT NULL,
  `rml_req_ip_imei_no` varchar(255) DEFAULT NULL,
  `rml_req_lat` varchar(255) DEFAULT NULL,
  `rml_req_long` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_msg_log`
--

INSERT INTO `request_msg_log` (`rml_req_msg_id`, `rml_req_type`, `rml_req_action`, `rml_login_details_id`, `rml_creation_date`, `rml_browser_used`, `rml_browser_version`, `rml_os_version`, `rml_os_info`, `rml_req_ip_imei_no`, `rml_req_lat`, `rml_req_long`) VALUES
(230, 'W', '/ContactManagement/rest/addContactDetail', 1, '2020-03-28 20:31:56', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(231, 'W', '/ContactManagement/rest/addContactDetail', 1, '2020-03-28 20:35:25', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(232, 'W', '/ContactManagement/rest/addContactDetail', 1, '2020-03-28 20:35:43', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(233, 'W', '/ContactManagement/rest/addContactDetail', 1, '2020-03-28 20:37:08', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(234, 'W', '/ContactManagement/home', 1, '2020-03-28 21:01:52', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(235, 'W', '/ContactManagement/home', 1, '2020-03-28 21:13:48', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(236, 'W', '/ContactManagement/rest/addContactDetail', 1, '2020-03-28 21:15:53', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(237, 'W', '/ContactManagement/rest/editContactDetail', 1, '2020-03-28 21:16:01', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(238, 'W', '/ContactManagement/rest/editContactDetail', 1, '2020-03-28 21:16:02', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(239, 'W', '/ContactManagement/rest/editContactDetail', 1, '2020-03-28 21:16:19', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(240, 'W', '/ContactManagement/rest/editContactDetail', 1, '2020-03-28 21:16:20', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(241, 'W', '/ContactManagement/rest/editContactDetail', 1, '2020-03-28 21:16:20', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(242, 'W', '/ContactManagement/rest/addContactDetail', 1, '2020-03-28 21:17:04', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(243, 'W', '/ContactManagement/rest/editContactDetail', 1, '2020-03-28 21:17:16', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(244, 'W', '/ContactManagement/rest/addContactDetail', 1, '2020-03-28 21:17:20', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(245, 'W', '/ContactManagement/rest/editContactDetail', 1, '2020-03-28 21:17:35', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(246, 'W', '/ContactManagement/rest/addContactDetail', 1, '2020-03-28 21:17:42', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(247, 'W', '/ContactManagement/home', 1, '2020-03-28 21:25:38', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(248, 'W', '/ContactManagement/rest/editContactDetail', 1, '2020-03-28 21:26:01', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(249, 'W', '/ContactManagement/home', 1, '2020-03-28 21:30:09', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(250, 'W', '/ContactManagement/home', 1, '2020-03-28 21:30:09', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(251, 'W', '/ContactManagement/rest/addContactDetail', 1, '2020-03-28 21:30:40', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(252, 'W', '/ContactManagement/rest/deleteContactDetail', 1, '2020-03-28 21:31:35', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(253, 'W', '/ContactManagement/home', 1, '2020-03-28 22:19:36', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(254, 'W', '/ContactManagement/home', 1, '2020-03-28 22:31:47', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(255, 'W', '/ContactManagement/rest/addContactDetail', 1, '2020-03-28 22:32:18', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(256, 'W', '/ContactManagement/rest/deleteContactDetail', 1, '2020-03-28 22:32:22', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL),
(257, 'W', '/ContactManagement/home', 1, '2020-03-29 11:52:15', 'Chrome', '80.0.3987.149', '10.0', 'Windows 10', '0:0:0:0:0:0:0:1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_master`
--

CREATE TABLE `role_master` (
  `rm_role_id` int(11) NOT NULL,
  `rm_role_name` varchar(250) NOT NULL,
  `rm_is_admin` char(1) NOT NULL,
  `rm_created_by` varchar(320) NOT NULL,
  `rm_creation_datetime` datetime NOT NULL,
  `rm_modified_by` varchar(320) DEFAULT NULL,
  `rm_modification_datetime` datetime DEFAULT NULL,
  `rm_is_active_flg` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_master`
--

INSERT INTO `role_master` (`rm_role_id`, `rm_role_name`, `rm_is_admin`, `rm_created_by`, `rm_creation_datetime`, `rm_modified_by`, `rm_modification_datetime`, `rm_is_active_flg`) VALUES
(1, 'admin', 'Y', 'admin', '2020-03-27 00:00:00', NULL, NULL, 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `captcha_dtl`
--
ALTER TABLE `captcha_dtl`
  ADD PRIMARY KEY (`cd_id`);

--
-- Indexes for table `login_details`
--
ALTER TABLE `login_details`
  ADD PRIMARY KEY (`ld_login_details_id`);

--
-- Indexes for table `mci_contact_dtls`
--
ALTER TABLE `mci_contact_dtls`
  ADD PRIMARY KEY (`contact_id`),
  ADD UNIQUE KEY `contact_last_name_unique_index` (`contact_last_name`) USING BTREE,
  ADD UNIQUE KEY `contact_mobile_number_unique_index` (`contact_mobile_number`),
  ADD UNIQUE KEY `contact_email_id_unique_index` (`contact_email_id`);

--
-- Indexes for table `request_msg_log`
--
ALTER TABLE `request_msg_log`
  ADD PRIMARY KEY (`rml_req_msg_id`);

--
-- Indexes for table `role_master`
--
ALTER TABLE `role_master`
  ADD PRIMARY KEY (`rm_role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `captcha_dtl`
--
ALTER TABLE `captcha_dtl`
  MODIFY `cd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login_details`
--
ALTER TABLE `login_details`
  MODIFY `ld_login_details_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mci_contact_dtls`
--
ALTER TABLE `mci_contact_dtls`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `request_msg_log`
--
ALTER TABLE `request_msg_log`
  MODIFY `rml_req_msg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT for table `role_master`
--
ALTER TABLE `role_master`
  MODIFY `rm_role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
