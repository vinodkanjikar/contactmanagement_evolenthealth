1. Refer "Evolent-Contact_Management_HLD_LLD_Vinod-Kanjikar.docx"  for Complete HLD & LLD design of Contact Management application. 

2. Prerequsite for this application execution
	a. JAVA 7 & above
	b. MySQL
	c. Apache Tomcat Application Server
	d. Google Chrome
	
	Note : During development of this application, Xampp web server solution stack package is used for faster release. 

3. Complete Database Dump is provided "contactmanagement.sql"

4. Application WAR has been provided for your reference.

5. Source Code is available in "vinodkanjikar-evolenthealth" folder.